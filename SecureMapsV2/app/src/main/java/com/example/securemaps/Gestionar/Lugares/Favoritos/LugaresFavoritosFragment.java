package com.example.securemaps.Gestionar.Lugares.Favoritos;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.securemaps.Api.ApiLugaresFavoritos;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.R;
import com.example.securemaps.Reportar.MapaPrincipalFragment;
import com.example.securemaps.Reportar.ReporteViewModel;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentLugaresFavoritosBinding;
import com.example.securemaps.databinding.ViewholderFavoritosBinding;
import com.google.android.gms.maps.model.Marker;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LugaresFavoritosFragment extends Fragment {
    private FragmentLugaresFavoritosBinding binding;
    private MapaPrincipalFragment mapaPrincipalFragment;
    private DialogConfirmarFragment dialogConfirmarFragment;
    private DialogViewModel dialogViewModel;
    private ReporteViewModel reporteViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentLugaresFavoritosBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);

        // Obtengo los lugares favoritos
        mostrarLugaresFavoritos();

        // Click en crear favorito
        binding.crearLugarFavorito.setOnClickListener(click -> {
            crearLugarFavorito();
        });
    }

    // RecyclerView
    class LugaresFavoritosAdapter extends RecyclerView.Adapter<LugaresFavoritosViewHolder> {
        private List<LugarFavorito> lugaresFavoritosList;

        @NonNull
        @Override
        public LugaresFavoritosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new LugaresFavoritosViewHolder(ViewholderFavoritosBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull LugaresFavoritosViewHolder holder, int position) {
            LugarFavorito lugarFavorito = lugaresFavoritosList.get(position);
            // Pongo la calle que le corresponde
            holder.binding.lugarFavorito.setText(lugarFavorito.calle);
            // Ver lugar en el mapa
            holder.binding.mostrarFavorito.setOnClickListener(click -> {
                mostrarFavorito(lugarFavorito);
            });
            // Modificar lugar
            holder.binding.modificarLugar.setOnClickListener(click -> {
                modificarLugar(lugarFavorito);
            });
            // Elimino el lugar
            holder.binding.eliminarFavorito.setOnClickListener(click -> {
                borrarLugar(lugarFavorito);
            });
        }

        @Override
        public int getItemCount() {
            if (lugaresFavoritosList == null) {
                return 0;
            } else {
                return lugaresFavoritosList.size();
            }
        }

        public void setLugaresFavoritosList(List<LugarFavorito> lugaresFavoritosList) {
            this.lugaresFavoritosList = lugaresFavoritosList;
        }
    }

    // View holder
    class LugaresFavoritosViewHolder extends RecyclerView.ViewHolder {
        ViewholderFavoritosBinding binding;

        public LugaresFavoritosViewHolder(ViewholderFavoritosBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    // Muestra todos los lugares favoritos
    private void mostrarLugaresFavoritos() {
        LugaresFavoritosAdapter lugaresFavoritosAdapter = new LugaresFavoritosAdapter();
        ApiLugaresFavoritos.api.getLugaresFavoritos().enqueue(new Callback<ApiLugaresFavoritos.Lugares>() {
            @Override
            public void onResponse(Call<ApiLugaresFavoritos.Lugares> call, Response<ApiLugaresFavoritos.Lugares> response) {
                // No hay datos
                if (response.body().results.size() == 0) {
                    binding.layoutNoHayDatos.setVisibility(View.VISIBLE);
                }
                // Hay datos
                else {
                    binding.layoutNoHayDatos.setVisibility(View.GONE);
                }
                // Paso los datos siempre, contenga o no lugares
                lugaresFavoritosAdapter.setLugaresFavoritosList(response.body().results);
                binding.listaLugaresFavoritos.setAdapter(lugaresFavoritosAdapter);
            }

            @Override
            public void onFailure(Call<ApiLugaresFavoritos.Lugares> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Crea un nuevo lugar favorito
    private void crearLugarFavorito() {
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.MODIFICAR);
        mapaPrincipalFragment = new MapaPrincipalFragment(new MapaPrincipalFragment.ClickButtonCallback() {
            @Override
            public void click(Marker lugarMarcado) {
                if (lugarMarcado != null) {
                    String calle = mapaPrincipalFragment.obtenerCalle(lugarMarcado.getPosition());
                    dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
                        @Override
                        public void clickButtonPositivo() {
                            // Inserto el lugar favorito
                            ApiLugaresFavoritos.api.insertLugarFavorito(new LugarFavorito(calle, lugarMarcado.getPosition().latitude, lugarMarcado.getPosition().longitude)).enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    // Insertado con exito
                                    if (response.code() == 200) {
                                        DynamicToast.makeSuccess(getContext(),getString(R.string.favorito_creado)).show();
                                        mostrarLugaresFavoritos();
                                    }
                                    // Error al insertar
                                    else if (response.code() == 400) {
                                        DynamicToast.makeError(getContext(), getString(R.string.favorito_no_creado)).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    ErrorCode.errorApi(getContext(), t.getMessage());
                                }
                            });
                            dialogConfirmarFragment.dismiss();
                            mapaPrincipalFragment.dismiss();
                        }
                    });
                    DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.crear_favorito), getString(R.string.favorito_texto) + calle + "?", getString(R.string.agregar));
                    dataDialogConfirmar.setIcon(R.drawable.ic_baseline_favorite_24);
                    dataDialogConfirmar.setColorIcon(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
                    dataDialogConfirmar.setColorTitle(getString(R.color.verdePrincipal));
                    dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

                    dialogConfirmarFragment.show(getChildFragmentManager(), "dialog");
                }
            }
        });

        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
    }

    // Muestra el lugar favorito en el mapa
    private void mostrarFavorito(LugarFavorito lugarFavorito) {
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.FAVORITO);
        mapaPrincipalFragment = new MapaPrincipalFragment(new MapaPrincipalFragment.ClickButtonCallback() {
            @Override
            public void click(Marker lugarMarcado) {
                // AQUI DENTRO NO VA NADA, YA QUE NO HACE NADA EL BOTON
            }
        });
        mapaPrincipalFragment.lugarFavorito = lugarFavorito;
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
    }

    // Modifica el lugar favorito
    private void modificarLugar(LugarFavorito lugarFavorito) {
        DynamicToast.make(getContext(), getString(R.string.modificar_lugares_etiquetados_instrucciones), Toast.LENGTH_LONG).show();
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.MODIFICAR);
        mapaPrincipalFragment = new MapaPrincipalFragment(new MapaPrincipalFragment.ClickButtonCallback() {
            @Override
            public void click(Marker lugarMarcado) {
                if (lugarMarcado != null) {
                    String calle = mapaPrincipalFragment.obtenerCalle(lugarMarcado.getPosition());
                    dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
                        @Override
                        public void clickButtonPositivo() {
                            // Actualizo los datos
                            ApiLugaresFavoritos.api.updateLugarFavorito(new LugarFavorito(lugarFavorito.id, calle, lugarMarcado.getPosition().latitude, lugarMarcado.getPosition().longitude)).enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    // Actualizado con exito
                                    if (response.code() == 200) {
                                        DynamicToast.makeSuccess(getContext(), getString(R.string.lugar_modificado)).show();
                                        mostrarLugaresFavoritos();
                                    }
                                    // Error al actualizar
                                    else if (response.code() == 400) {
                                        DynamicToast.makeError(getContext(), getString(R.string.error_lugar_modificado)).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    ErrorCode.errorApi(getContext(), t.getMessage());
                                }
                            });
                            dialogConfirmarFragment.dismiss();
                            mapaPrincipalFragment.dismiss();
                        }
                    });
                    DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.modificar), getString(R.string.modificar_lugar_favorito) + calle + "?", getString(R.string.modificar));
                    dataDialogConfirmar.setIcon(R.drawable.ic_baseline_edit_24);
                    dataDialogConfirmar.setColorIcon(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
                    dataDialogConfirmar.setColorTitle(getString(R.color.verdePrincipal));
                    dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);
                    dialogConfirmarFragment.show(getChildFragmentManager(), "dialog");
                }
            }
        });
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");

    }

    // Borra un lugar favorito
    private void borrarLugar(LugarFavorito lugarFavorito) {
        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                // Elimino el lugar favorito
                ApiLugaresFavoritos.api.deleteLugarFavorito(lugarFavorito.id).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Eliminado con exito
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(getContext(), getString(R.string.lugarFavoritoEliminado)).show();
                            mostrarLugaresFavoritos();
                        }

                        // Error al eliminar
                        else if (response.code() == 400) {
                            DynamicToast.makeError(getContext(), getString(R.string.error_eliminar_lugar_favorito)).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogConfirmarFragment.dismiss();
            }
        });

        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar), getString(R.string.eliminar_favorito_texto), getString(R.string.eliminar));
        dataDialogConfirmar.setIcon(R.drawable.eliminar);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
        dataDialogConfirmar.setColorBackgroundPositivo(getContext().getDrawable(R.drawable.button_eliminar_positivo));

        dataDialogConfirmar.setColorBotonNegativo(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundNegativo(getContext().getDrawable(R.drawable.button_eliminar_negativo));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);
        dialogConfirmarFragment.show(getFragmentManager(), "dialog");
    }
}
package com.example.securemaps.Gestionar.Lugares.Etiquetados;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.securemaps.Api.Data;
import com.google.gson.annotations.SerializedName;

public class LugarEtiquetado {
    public int id;
    @SerializedName(Data.LUGAR)
    public String lugar;
    @SerializedName(Data.CALLE)
    public String calle;
    @SerializedName(Data.LATITUDE)
    public double latitude;
    @SerializedName(Data.LONGITUDE)
    public double longitude;

    public LugarEtiquetado() {}

    public LugarEtiquetado(String lugar) {
        this.lugar = lugar;
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

    public LugarEtiquetado(String calle, double latitude, double longitude, String lugar) {
        this.calle = calle;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lugar = lugar;
    }

    public LugarEtiquetado(int id,  String calle, double latitude, double longitude) {
        this.id = id;
        this.calle = calle;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

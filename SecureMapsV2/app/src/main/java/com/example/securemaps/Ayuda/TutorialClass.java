package com.example.securemaps.Ayuda;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TutorialClass {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String ejecutado;

    public TutorialClass(String ejecutado) {
        this.ejecutado = ejecutado;
    }
}

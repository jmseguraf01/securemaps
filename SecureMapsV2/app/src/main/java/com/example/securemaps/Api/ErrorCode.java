package com.example.securemaps.Api;


import android.content.Context;
import android.widget.Toast;

import com.example.securemaps.R;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

public class ErrorCode {
    public static final int code = 204;
    public static final int valorDuplicado = 23505;
    public static final int valorRequerido = 23502;

    public static void errorApi(Context context, String error) {
        DynamicToast.makeError(context, context.getString(R.string.error_api) + error, Toast.LENGTH_LONG).show();
    }

}

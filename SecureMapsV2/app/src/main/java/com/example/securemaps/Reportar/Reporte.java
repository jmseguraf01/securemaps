package com.example.securemaps.Reportar;

import com.example.securemaps.Api.Data;
import com.google.gson.annotations.SerializedName;

public class Reporte {
    @SerializedName(Data.ID)
    public int id;
    @SerializedName(Data.ID_USUARIO)
    public int idUsuario;
    @SerializedName(Data.CALLE)
    public String calle;
    @SerializedName(Data.CIUDAD)
    public String ciudad;
    @SerializedName(Data.LATITUDE)
    public double latitude;
    @SerializedName(Data.LONGITUDE)
    public double longitude;
    @SerializedName(Data.COMENTARIO)
    public String comentario;
    @SerializedName(Data.ANONIMO)
    public boolean anonimo;


    public Reporte(String calle, String ciudad,  double latitude, double longitude, String comentario, boolean anonimo) {
        this.calle = calle;
        this.ciudad = ciudad;
        this.latitude = latitude;
        this.longitude = longitude;
        this.comentario = comentario;
        this.anonimo = anonimo;
    }

    public Reporte(int id, String calle, String ciudad, double latitude, double longitude, String comentario) {
        this.id = id;
        this.calle = calle;
        this.ciudad = ciudad;
        this.latitude = latitude;
        this.longitude = longitude;
        this.comentario = comentario;
    }
}

package com.example.securemaps.Registro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.securemaps.Api.ApiLogin;
import com.example.securemaps.Api.ApiRegistro;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Login.User;
import com.example.securemaps.R;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.SignupTabFragmentBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupTabFragment extends BaseFragment {
    private SignupTabFragmentBinding binding;
    private Utils utils = new Utils();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = SignupTabFragmentBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Click el el boton registrar
        binding.button.setOnClickListener(click -> {
            String usuario = binding.name.getText().toString();
            String email = binding.email.getText().toString();
            String password = binding.password.getText().toString();
            String passwordRepetir = binding.passwordConfirm.getText().toString();
            // Siempre que todos los campos esten rellenados
            if (!usuario.equals("") && !email.equals("") && !password.equals("") && !passwordRepetir.equals("")) {
                // Siempre que las dos contraseñas coincidan
                if (password.equals(passwordRepetir) && utils.validarEmail(email)) {
                    registrarCuenta(usuario, email, password);
                }
                // El email no tiene formato correcto
                else if (!utils.validarEmail(email)) {
                    binding.email.setError(getString(R.string.campo_formato_email));
                }
                // Las contraseñas no coinciden
                else if (!password.equals(passwordRepetir)){
                    DynamicToast.makeWarning(getContext(), getString(R.string.contrasenyas_no_coinciden)).show();
                }
            }
            // Hay campos vacios
            else {
                if (usuario.isEmpty()) {
                    binding.name.setError(getString(R.string.campo_obligatorio));
                } if (email.isEmpty()) {
                    binding.email.setError(getString(R.string.campo_obligatorio));
                } if (password.isEmpty()) {
                    binding.password.setError(getString(R.string.campo_obligatorio));
                } if (passwordRepetir.isEmpty()) {
                    binding.passwordConfirm.setError(getString(R.string.campo_obligatorio));
                }
            }
        });

        // Cuando se registra una cuenta nueva
        smViewModel.codeRegistro.observe(getViewLifecycleOwner(), code -> {
            if (code == null) {
                navController.navigate(R.id.go_to_MapaPrincipalFragment);
            }
        });
    }

    // Registrar un nuevo usuario
    public void registrarCuenta(String usuario, String email, String password) {
        ApiRegistro.api.add(new User(usuario, email, password, true)).enqueue(new Callback<ApiLogin.Respuesta>() {
            @Override
            public void onResponse(Call<ApiLogin.Respuesta> call, Response<ApiLogin.Respuesta> response) {
                // Registrado con exito
                if (response.code() == 200) {
                    // Siempre que no haya ningun error
                    if (response.body().error == null) {
                        DynamicToast.makeSuccess(getContext(), getString(R.string.usuario_registrado)).show();
                        utils.guardarUsuario(response.body().id, response.body().usuario, response.body().email, response.body().password, true);
                        utils.crearLugaresEtiquetados();
                        navController.navigate(R.id.go_to_MapaPrincipalFragment);
                    }
                    // Error valor repetido
                    else if ( Integer.parseInt(response.body().error) == ErrorCode.valorDuplicado) {
                        DynamicToast.makeWarning(getContext(), getString(R.string.error_email_duplicado), Toast.LENGTH_LONG).show();
                    }
                    else {
                        DynamicToast.makeError(getContext(), getString(R.string.error_titulo) + " : " + response.body().error, Toast.LENGTH_LONG).show();
                    }
                }
                // Error desconocido
                else if (response.code() == 400) {
                    DynamicToast.makeError(getContext(), getString(R.string.error_registar_usuario), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ApiLogin.Respuesta> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }
}

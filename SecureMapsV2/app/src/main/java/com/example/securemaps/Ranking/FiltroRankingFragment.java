package com.example.securemaps.Ranking;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.securemaps.Utils.SMViewModel;
import com.example.securemaps.databinding.FragmentFiltroRankingBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;


public class FiltroRankingFragment extends BottomSheetDialogFragment {
    private FragmentFiltroRankingBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return (binding = FragmentFiltroRankingBinding.inflate(inflater, container, false)).getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Ordenar ascendente
        binding.buttonAsc.setOnClickListener(click -> {
            SMViewModel.ordenRankingReportes.setValue(RankingFragment.Orden.DESCENDIENTE);
            dismiss();
        });

        // Modificar comentario
        binding.buttonDesc.setOnClickListener(click -> {
            SMViewModel.ordenRankingReportes.setValue(RankingFragment.Orden.ASCENDIENTE);
            dismiss();
        });
    }
}
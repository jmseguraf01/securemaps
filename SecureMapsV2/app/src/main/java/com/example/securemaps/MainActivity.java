 package com.example.securemaps;

 import android.content.Intent;
 import android.content.pm.ActivityInfo;
 import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
import com.example.securemaps.Api.ApiUsuario;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Ayuda.Tutorial;
import com.example.securemaps.Utils.SMViewModel;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.ActivityMainBinding;
import com.example.securemaps.databinding.DrawerHeaderBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Callback;


 public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private static DrawerHeaderBinding drawerHeaderBinding;
    private Utils utils;
    private SMViewModel smViewModel;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView((binding = ActivityMainBinding.inflate(getLayoutInflater())).getRoot());
        drawerHeaderBinding = DrawerHeaderBinding.bind(binding.navView.getHeaderView(0));
        smViewModel = smViewModel = new ViewModelProvider(this).get(SMViewModel.class);
         setSupportActionBar(binding.toolbar);
        // Los fragments que meto aqui, saldra el boton de hamburguesa
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.mapaPrincipalFragment, R.id.gestionarReportesFragment,
                R.id.configuracionFragment, R.id.lugaresEtiquetadosFragment,
                R.id.lugaresFavoritosFragment, R.id.rankingFragment)
                .setOpenableLayout(binding.drawerLayout)
                .build();


        NavController navController = ((NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment)).getNavController();
        NavigationUI.setupWithNavController(binding.navView, navController);
        NavigationUI.setupWithNavController(binding.toolbar, navController, appBarConfiguration);

         // Configuro el menu del bottom
         binding.bottomNavView.add(new MeowBottomNavigation.Model(1, R.drawable.ic_baseline_location_on_24));
         binding.bottomNavView.add(new MeowBottomNavigation.Model(2, R.drawable.ic_baseline_favorite_24));
         binding.bottomNavView.show(1, true);

         // Click en el menu del bottom
         binding.bottomNavView.setOnClickMenuListener(new Function1<MeowBottomNavigation.Model, Unit>() {
             @Override
             public Unit invoke(MeowBottomNavigation.Model model) {
                 switch (model.getId()){
                     case 1:
                         navController.navigate(R.id.lugaresEtiquetadosFragment);
                         break;

                     case 2:
                         navController.navigate(R.id.lugaresFavoritosFragment);
                         break;
                 }
                 return null;
             }
         });


         // Para que no se mueva el layout cuando se muestra el teclado
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

         utils = new Utils();
         Utils.context = this;

         // Si el tutorial no ha sido ejecutado todavia
         if (!utils.tutorialEjecutado(this)) {
             Intent intentIntro = new Intent(this, IntroActivity.class);
             startActivity(intentIntro);
         }

        // Oculto lo que quiera de los navControler
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller,
                                             @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                idCurrentDestination = destination.getId();

                // Escondo el toolbar en el login
                if(destination.getId() == R.id.loginFragment || destination.getId() == R.id.registroFragment) {
                    binding.toolbar.setVisibility(View.GONE);
                    binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else {
                    binding.toolbar.setVisibility(View.VISIBLE);
                    binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }

                // Muestro el bottom drawer en gestionar lugares
                if (destination.getId() == R.id.lugaresEtiquetadosFragment || destination.getId() == R.id.lugaresFavoritosFragment) {
                    binding.bottomNavView.setVisibility(View.VISIBLE);
                } else {
                    binding.bottomNavView.setVisibility(View.GONE);
                }
            }
        });

        // Click en el boton ayuda
        binding.toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.itemAyuda) {
                Tutorial tutorial = new Tutorial(this);
                // Identifico cual es la pantalla en la que se ha dado al boton ayuda
                int id = navController.getCurrentDestination().getId();

                if (id == R.id.mapaPrincipalFragment) {
                    tutorial.mapaPrincipal();
                } else if (id == R.id.gestionarReportesFragment) {
                    // Siempre que no este en el modo de buscar reporte
                    if (findViewById(R.id.linearBuscarReportes).getVisibility() == View.GONE) {
                        // Siempre que exista algun reporte
//                        if (reporteViewModel.reportesList.getValue().getValue().size() > 0) {
//                            tutorial.gestionarReportes(0);
//                        }
//                        // Si no existe ningun reporte
//                        else {
//                            DynamicToast.makeWarning(this, getString(R.string.error_tutorial_reportes), Toast.LENGTH_LONG).show();
//                        }
                    }
                    // Si esta en el modo de "buscar reporte"
                    else if (findViewById(R.id.linearBuscarReportes).getVisibility() == View.VISIBLE){
                        tutorial.gestionarReportes(1);
                    }
                } else if (id == R.id.lugaresEtiquetadosFragment) {
                    tutorial.lugaresEtiquetados();
                } else if (id == R.id.lugaresFavoritosFragment) {
                    // Siempre que haya algun lugar favorito
//                    if (lugaresFavoritosViewModel.lugaresFavoritosList.getValue().getValue().size() > 0) {
//                        tutorial.lugaresFavoritos(0);
//                    }

                    // Si no existe ningun lugar favorito, solo muestro el tutorial del de crear lugar
//                    else {
//                        tutorial.lugaresFavoritos(1);
//                    }
                } else if (id == R.id.configuracionFragment) {
                    tutorial.configuracion();
                }
            }

            // Click en el boton compartir
            else if (item.getItemId() == R.id.itemCompartir) {
                compartirLayout();
            }

            return false;
        });

        // Click en el boton de la imagen de usuario
        drawerHeaderBinding.imageView.setOnClickListener(click -> {
            lanzadorGaleria.launch(new String[]{"image/*"});
        });

        // Cuando la imagen del usuario cambia
        smViewModel.imagenUsuario.observe(this, new Observer<byte[]>() {
            @Override
            public void onChanged(byte[] bytes) {
                actualizarImagen(bytes);
            }
        });

        // Si tengo que poner la imagen en el drawer
        smViewModel.actualizarImagen.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean cambiarImagen) {
                if (cambiarImagen) {
                    actualizarImagen();
                    smViewModel.actualizarImagen.setValue(false);
                }
            }
        });
    }

     // Funcion que se ejecuta cuando se haya escogido una imagen para el usuario
     private final ActivityResultLauncher<String[]> lanzadorGaleria = registerForActivityResult(new ActivityResultContracts.OpenDocument(), uri -> {
         try {
             smViewModel.imagenUsuario.postValue(uri2Bytes(uri));
         } catch (IOException e) {
             e.printStackTrace();
         }

     });

     // Creo el menu de la toolba arriba
     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         MenuInflater inflater = getMenuInflater();
         inflater.inflate(R.menu.top_menu, menu);
         return true;
     }

     // Funcion que muestra el layout de compartir
     private void compartirLayout() {
         Intent myIntent = new Intent(Intent.ACTION_SEND);
         myIntent.setType("text/plain");
         String body = getString(R.string.bodyCompartir);
         myIntent.putExtra(Intent.EXTRA_TEXT,body);
         startActivity(Intent.createChooser(myIntent, "Share Using"));
     }

     // Cambia el nombre del usuario del drawer
     public void actualizarUsuarioDrawer(String usuario) {
         drawerHeaderBinding.usuarioDrawer.setText(usuario);
     }

     // Cambia la imagen del usuario
     private void actualizarImagen(byte[] bytes) {
         MultipartBody.Part part = MultipartBody.Part.createFormData("avatar", "avatar", RequestBody.create(MediaType.parse("image/*"), bytes));
         ApiUsuario.api.postImage(part).enqueue(new Callback<Void>() {
             @Override
             public void onResponse(retrofit2.Call<Void> call, retrofit2.Response<Void> response) {
                 // Imagen guardada con exito
                 if (response.code() == 200) {
                     actualizarImagen();
                     DynamicToast.makeSuccess(getApplicationContext(), getString(R.string.imagen_guardada)).show();
                 }
                 // Error al guardar la imagen
                 else {
                     DynamicToast.makeError(getApplicationContext(), getString(R.string.error_guardar_imagen)).show();
                 }
             }

             @Override
             public void onFailure(retrofit2.Call<Void> call, Throwable t) {
                 ErrorCode.errorApi(getApplicationContext(), t.getMessage());
             }

         });
     }

     public void actualizarImagen() {
         Glide.with(getApplicationContext()).load(ApiUsuario.RUTA_GET_IMAGE +  utils.obtenerIdUsuario())
                 .error(R.drawable.ic_baseline_person_24)
                 .into(drawerHeaderBinding.imageView);
     }

     byte[] uri2Bytes(Uri uri) throws IOException {
         InputStream inputStream = getContentResolver().openInputStream(uri);
         ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
         byte[] buffer = new byte[1024];
         for(int len; (len = inputStream.read(buffer)) != -1;) byteBuffer.write(buffer, 0, len);
         return byteBuffer.toByteArray();
     }

     // Al pulsar atras se cierra la aplicacion
     @Override
     public void onBackPressed() {
         finish();
     }
 }
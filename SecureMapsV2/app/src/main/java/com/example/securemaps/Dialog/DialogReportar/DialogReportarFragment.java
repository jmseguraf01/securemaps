package com.example.securemaps.Dialog.DialogReportar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.securemaps.R;
import com.example.securemaps.databinding.FragmentDialogReportarBinding;

public class DialogReportarFragment extends DialogFragment {
    private FragmentDialogReportarBinding binding;
    public String title;
    ClickDialogReportarCallback clickDialogReportarCallback;

    public interface ClickDialogReportarCallback {
        void clickButtonReportar(String comentario, boolean comentarioAnonimo);
    }

    // Constructor en el que le paso el titulo
    public DialogReportarFragment(String title, ClickDialogReportarCallback clickDialogReportarCallback) {
        this.title = title;
        this.clickDialogReportarCallback = clickDialogReportarCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentDialogReportarBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);
        // Poner el fragment del dialog redondo
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
        // Actualizo el titulo
        binding.titulo.setText(title);
        // Click en el switch
        binding.switchDialogReportar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    binding.editText.setVisibility(View.VISIBLE);
                } else if (!isChecked) {
                    binding.editText.setVisibility(View.GONE);
                }
            }
        });

        // Click en el boton reportar
        binding.botonPositivo.setOnClickListener(c -> {
            clickDialogReportarCallback.clickButtonReportar(binding.editText.getText().toString(), binding.checkboxReportarAnonimo.isChecked());
        });

        // Click boton negativo
        binding.botonNegativo.setOnClickListener(c -> {
            dismiss();
        });

    }
}
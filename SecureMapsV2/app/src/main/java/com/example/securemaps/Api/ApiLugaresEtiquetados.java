package com.example.securemaps.Api;

import com.example.securemaps.Gestionar.Lugares.Etiquetados.LugarEtiquetado;
import com.example.securemaps.Gestionar.Lugares.Favoritos.LugarFavorito;
import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Utils.Utils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ApiLugaresEtiquetados {
    private static final String RUTA_INSERTS = "insertLugares-etiquetados";
    private static final String RUTA_GET = "getlugares-etiquetados";
    private static final String RUTA_UPDATE = "updatelugares-etiquetados";
    private static final String RUTA_DELETE = "deletelugares-etiquetados";

    public class Lugares {
        public List<LugarEtiquetado> results;
    }

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();
                            HttpUrl url = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Data.CAMPO_EMAIL, new Utils().obtenerEmail())
                                    .addQueryParameter(Data.CAMPO_PASSWORD, new Utils().obtenerPassword())
                                    .build();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .url(url);
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    }).build()
            )
            .build()
            .create(Api.class);

    public interface Api {
        @POST(RUTA_INSERTS)
        Call<Void> inserts();

        @GET(RUTA_GET)
        Call<Lugares> getLugaresEtiquetados();

        @POST(RUTA_UPDATE)
        Call<Void> updateLugaresEtiquetados(@Body LugarEtiquetado lugarEtiquetado);
        @GET(RUTA_DELETE)
        Call<Void> deleteLugaresEtiquetados(@Query(Data.ID) int id);
    }

}

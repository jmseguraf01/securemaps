package com.example.securemaps.Api;

import com.example.securemaps.Login.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public class ApiRegistro {
    private static final String RUTA = "addUser";

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api.class);


    public interface Api {
        @POST(RUTA)
        Call<ApiLogin.Respuesta> add(@Body User user);
    }
}

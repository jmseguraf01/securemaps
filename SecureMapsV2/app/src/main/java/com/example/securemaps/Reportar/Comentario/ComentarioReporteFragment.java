package com.example.securemaps.Reportar.Comentario;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.securemaps.Api.ApiComentario;
import com.example.securemaps.Api.ApiLogin;
import com.example.securemaps.Api.ApiUsuario;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;
import com.example.securemaps.Dialog.DialogModificar.DialogModificarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.R;
import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.Utils.SMViewModel;
import com.example.securemaps.databinding.FragmentComentarioReporteBinding;
import com.example.securemaps.databinding.ViewholderComentariosBinding;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComentarioReporteFragment extends BaseFragment {
    private FragmentComentarioReporteBinding binding;
    private ComentariosAdapter comentariosAdapter;
    private DialogViewModel dialogViewModel;
    private DialogConfirmarFragment dialogConfirmarFragment;
    private Reporte reporteActual;
    private DialogModificarFragment dialogModificarFragment;
    private BottomSheetDialog dialog;
    private OpcionesComentarios opcionesComentarios;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentComentarioReporteBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        reporteActual = smViewModel.reporteComentario;
        // Siempre que el reporte se haya cogido
        if (reporteActual != null) {
            // Pongo de titulo la calle del lugar reportado
            getActivity().setTitle("Reporte: " + reporteActual.calle);
            actualizarDatos(reporteActual);
        }

        // Click enviar comentario
        binding.buttonEnviarComentario.setOnClickListener(click ->  {
            enviarComentario(binding.editTextComentario.getText().toString());
        });

        // Actualizo los datos
        SMViewModel.actualizarComentarios.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    actualizarDatos(reporteActual);
                }
            }
        });
    }

    // Recyclerview
    class ComentariosAdapter extends RecyclerView.Adapter<ComentariosViewHolder> {
        private List<Comentario> comentarioList = new ArrayList<>();

        @NonNull
        @NotNull
        @Override
        public ComentariosViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new ComentariosViewHolder(ViewholderComentariosBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull ComentariosViewHolder holder, int position) {
            Comentario comentario = comentarioList.get(position);
            holder.binding.textViewComentario.setText(comentario.comentario);
            // Obtengo el nombre de usuario
            ApiUsuario.api.getUsername(comentario.idUsuario).enqueue(new Callback<ApiLogin.Respuesta>() {
                @Override
                public void onResponse(Call<ApiLogin.Respuesta> call, Response<ApiLogin.Respuesta> response) {
                    // Obtengo el usuari con exito
                    if (response.code() == 200) {
                        // Siempre que contenga datos
                        if (response.body() != null) {
                            holder.binding.textViewUsuario.setText(response.body().usuario);
                        }
                    }
                    // Error al obtener el usuario
                    else {
                        DynamicToast.makeWarning(getContext(), getString(R.string.error_obtener_usuario)).show();
                    }
                }

                @Override
                public void onFailure(Call<ApiLogin.Respuesta> call, Throwable t) {
                    ErrorCode.errorApi(getContext(), t.getMessage());
                }
            });
            // Pongo la imagen del usuario
            Glide.with(getContext()).load(ApiUsuario.RUTA_GET_IMAGE +  comentario.idUsuario)
                    .error(R.drawable.ic_baseline_person_24)
                    .into(holder.binding.imageViewUsuario);
            // Si el comentario es del usuario, muestro los botones de eliminar y modificar
            if (comentario.idUsuario == Integer.parseInt(utils.obtenerIdUsuario())) {
                holder.binding.optionComent.setVisibility(View.VISIBLE);
            }

            // Click en los 3 puntitos
            holder.binding.optionComent.setOnClickListener(click -> {
                SMViewModel.comentarioOpciones = comentario;
                opcionesComentarios = new OpcionesComentarios();
                opcionesComentarios.show(getParentFragmentManager(), "");
            });

        }

        @Override
        public int getItemCount() {
            if (comentarioList == null) {
                return 0;
            } else {
                return comentarioList.size();
            }
        }

        public void setComentarioList(List<Comentario> comentarioList) {
            this.comentarioList = comentarioList;
            notifyDataSetChanged();
        }

        public void addToList(Comentario comentario) {
            this.comentarioList.add(comentario);
            notifyDataSetChanged();
        }

        public void deleteToList(Comentario comentario) {
            this.comentarioList.remove(comentario);
            notifyDataSetChanged();
        }
    }

    class ComentariosViewHolder extends RecyclerView.ViewHolder {

        ViewholderComentariosBinding binding;

        public ComentariosViewHolder(ViewholderComentariosBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

    // Pongo los datos del lugar reportado
    private void actualizarDatos(Reporte reporte) {
        // Comentario vacio
        if (reporte.comentario.isEmpty()) {
            binding.textViewComentario.setText(getString(R.string.sin_comentario));
        }
        // Pongo el comentario
        else {
            binding.textViewComentario.setText(reporte.comentario);
        }
        // No anonimo
        if (!reporte.anonimo) {
            // Obtengo el nombre de usuario
            ApiUsuario.api.getUsername(reporte.idUsuario).enqueue(new Callback<ApiLogin.Respuesta>() {
                @Override
                public void onResponse(Call<ApiLogin.Respuesta> call, Response<ApiLogin.Respuesta> response) {
                    // Obtengo el usuari con exito
                    if (response.code() == 200) {
                        // Siempre que contenga datos
                        if (response.body() != null) {
                            binding.textViewUsuario.setText(response.body().usuario);
                        }
                    }
                    // Error al obtener el usuario
                    else {
                        DynamicToast.makeWarning(getContext(), getString(R.string.error_obtener_usuario)).show();
                    }
                }

                @Override
                public void onFailure(Call<ApiLogin.Respuesta> call, Throwable t) {
                    ErrorCode.errorApi(getContext(), t.getMessage());
                }
            });
            // Pongo la imagen del usuario
            Glide.with(getContext()).load(ApiUsuario.RUTA_GET_IMAGE +  reporte.idUsuario)
                    .error(R.drawable.ic_baseline_person_24)
                    .into(binding.imageViewUsuario);
        }
        // Anonimo
        else {
            binding.textViewUsuario.setText(getString(R.string.anonimo));
        }
        // Pongo la lista de comentarios
        comentariosAdapter = new ComentariosAdapter();
        binding.listaComentarios.setAdapter(comentariosAdapter);
        // Obtengo todos los comentarios
        ApiComentario.api.obtenerComentarios(reporte.id).enqueue(new Callback<ApiComentario.ListaComentarios>() {
            @Override
            public void onResponse(Call<ApiComentario.ListaComentarios> call, Response<ApiComentario.ListaComentarios> response) {
                if (response.code() == 200) {
                    comentariosAdapter.setComentarioList(response.body().results);
                } else {
                    DynamicToast.makeError(getContext(), getString(R.string.error_obtener_comentarios)).show();
                }
            }

            @Override
            public void onFailure(Call<ApiComentario.ListaComentarios> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Envia un comentario al reporte
    private void enviarComentario(String textoComentario) {
        Comentario comentario = new Comentario(smViewModel.reporteComentario.id, Integer.parseInt(utils.obtenerIdUsuario()), textoComentario);
        ApiComentario.api.insertarComentario(comentario).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                // Comentario insertado
                if (response.code() == 200) {
                    // Actualizo los datos
                    actualizarDatos(reporteActual);
                    // Oculto el teclado
                    utils.ocultarTeclado(getActivity());
                    // Vacio el edittext
                    binding.editTextComentario.setText("");
                    DynamicToast.makeSuccess(getContext(), "Comentario guardado").show();
                }
                // Error al añadir el comentario
                else {
                    DynamicToast.makeError(getContext(), getString(R.string.error_insertar_comentario)).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Elimina el comentario
    private void borrarComentario(Comentario comentario) {
        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                ApiComentario.api.deleteComentario(comentario).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        System.out.println(comentario.id);
                        // Eliminado con exito
                        if (response.code() == 200) {
                            // Actualizo los datos
                            actualizarDatos(reporteActual);
                            DynamicToast.makeSuccess(getContext(), getString(R.string.comentario_eliminado)).show();
                        }
                        // Error al eliminar
                        else {
                            DynamicToast.makeError(getContext(), getString(R.string.error_eliminar_comentario)).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogConfirmarFragment.dismiss();
            }
        });

        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar), getString(R.string.eliminar_comentario), getString(R.string.eliminar));
        dataDialogConfirmar.setIcon(R.drawable.eliminar);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
        dataDialogConfirmar.setColorBackgroundPositivo(getContext().getDrawable(R.drawable.button_eliminar_positivo));
        dataDialogConfirmar.setColorBotonNegativo(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundNegativo(getContext().getDrawable(R.drawable.button_eliminar_negativo));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);
        dialogConfirmarFragment.show(getFragmentManager(), "Dialog");
    }

    // Modifica un comentario
    private void modificarComentario(Comentario comentario) {
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            @Override
            public void clickButtonPositivo(String textoEditText) {
                comentario.comentario = textoEditText;
                ApiComentario.api.updateComentario(comentario).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Actualizado con exito
                        if (response.code() == 200) {
                            actualizarDatos(reporteActual);
                            DynamicToast.makeSuccess(getContext(), getString(R.string.comentario_modificado)).show();
                        } 
                        // Error al actualizar
                        else {
                            DynamicToast.makeError(getContext(), getString(R.string.error_actualizar_comentario)).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogModificarFragment.dismiss();
            }
        });
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.comentarios), getString(R.string.texto_comentarios), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dataDialogModificar.setTextoEdittext(comentario.comentario);
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
        dialogModificarFragment.show(getFragmentManager(), "Dialog");
    }

}
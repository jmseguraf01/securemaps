package com.example.securemaps.Gestionar.Lugares.Favoritos;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.securemaps.Api.Data;
import com.google.gson.annotations.SerializedName;

public class LugarFavorito {
    public int id;
    @SerializedName(Data.CALLE)
    public String calle;
    @SerializedName(Data.LATITUDE)
    public double latitude;
    @SerializedName(Data.LONGITUDE)
    public double longitude;

    public LugarFavorito(int id, String calle, double latitude, double longitude) {
        this.id = id;
        this.calle = calle;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LugarFavorito(String calle, double latitude, double longitude) {
        this.calle = calle;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

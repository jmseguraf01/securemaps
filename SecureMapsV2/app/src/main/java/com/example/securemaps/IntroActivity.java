package com.example.securemaps;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.appintro.AppIntro;
import com.github.appintro.AppIntroFragment;
import com.github.appintro.model.SliderPage;

public class IntroActivity extends AppIntro {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setProgressIndicator();
        setSkipButtonEnabled(false);
        showStatusBar(false);

        SliderPage pagina1 = new SliderPage();
        pagina1.setTitleTypefaceFontRes(R.font.gabriela);
        pagina1.setTitle(getString(R.string.title_pagina1));
        pagina1.setTitleColor(Color.parseColor("#1e9412"));
        pagina1.setDescription(getString(R.string.descripcion_pagina1));
        pagina1.setDescriptionTypefaceFontRes(R.font.gabriela);
        pagina1.setImageDrawable(R.drawable.imagen_pagina1);
        pagina1.setBackgroundDrawable(R.drawable.degradado_pagina1);

        SliderPage pagina2 = new SliderPage();
        pagina2.setTitleTypefaceFontRes(R.font.gabriela);
        pagina2.setTitle(getString(R.string.title_pagina2));
        pagina2.setTitleColor(Color.parseColor("#0016d1"));
        pagina2.setDescription(getString(R.string.descripcion_pagina2));
        pagina2.setDescriptionTypefaceFontRes(R.font.gabriela);
        pagina2.setImageDrawable(R.drawable.imagen_pagina2);
        pagina2.setBackgroundDrawable(R.drawable.degradado_pagina2);


        SliderPage pagina3 = new SliderPage();
        pagina3.setTitle(getString(R.string.title_pagina3));
        pagina3.setTitleTypefaceFontRes(R.font.gabriela);
        pagina3.setTitleColor(Color.parseColor("#8d8b00"));
        pagina3.setDescriptionTypefaceFontRes(R.font.gabriela);
        pagina3.setDescription(getString(R.string.descripcion_pagina3));
        pagina3.setImageDrawable(R.drawable.imagen_pagina3);
        pagina3.setBackgroundDrawable(R.drawable.degradado_pagina3);

        addSlide(AppIntroFragment.newInstance(pagina1));
        addSlide(AppIntroFragment.newInstance(pagina2));
        addSlide(AppIntroFragment.newInstance(pagina3));

    }

    // Cuando acaba, cierro el activity
    @Override
    public void onDonePressed(Fragment currentFragment) {
        finish();
    }
}

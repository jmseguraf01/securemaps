package com.example.securemaps.Api;

import com.example.securemaps.Login.User;
import com.example.securemaps.Utils.Utils;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public class ApiUsuario {
    private static final String RUTA_UPDATE = "updateUser";
    private static final String RUTA_DELETE_ACCOUNT = "deleteUser";
    private static final String RUTA_UPDATE_PASSWORD = "updateUserPassword";
    private static final String RUTA_GET_USERNAME = "getUsername";
    public static final String RUTA_GET_IMAGE = Data.URL + "uploads/";
    private static final String RUTA_UPDATE_NOTIFICACION_REPORTE = "updateNotificacionReporte";


    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();
                            HttpUrl url = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Data.CAMPO_EMAIL, new Utils().obtenerEmail())
                                    .addQueryParameter(Data.CAMPO_PASSWORD, new Utils().obtenerPassword())
                                    .build();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .url(url);
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    }).build()
            )
            .build()
            .create(Api.class);

    public interface Api {
        @POST(RUTA_UPDATE)
        Call<Void> updateUser(@Body User user);

        @GET(RUTA_DELETE_ACCOUNT)
        Call<Void> deleteAccount();

        @POST(RUTA_UPDATE_PASSWORD)
        Call<Void> updateUserPassword(@Body User user);

        @Multipart
        @POST("/upload")
        Call<Void> postImage(@Part MultipartBody.Part image);

        @GET(RUTA_GET_USERNAME)
        Call<ApiLogin.Respuesta> getUsername(@Query(Data.ID) int id);

        @POST(RUTA_UPDATE_NOTIFICACION_REPORTE)
        Call<Void> updateNotifacionReporte(@Body User user);

    }
}

package com.example.securemaps.Api;

import com.example.securemaps.Reportar.Comentario.Comentario;
import com.example.securemaps.Utils.Utils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ApiComentario {
    private static final String RUTA_ADD = "addComentarioReporte";
    private static final String RUTA_GET = "getComentariosReporte";
    private static final String RUTA_DELETE = "deleteComentarioReporte";
    private static final String RUTA_UPDATE = "updateComentarioReporte";

    public class ListaComentarios {
        public List<Comentario> results;
    }

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();
                            HttpUrl url = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Data.CAMPO_EMAIL, new Utils().obtenerEmail())
                                    .addQueryParameter(Data.CAMPO_PASSWORD, new Utils().obtenerPassword())
                                    .build();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .url(url);
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    }).build()
            )
            .build()
            .create(Api.class);

    public interface Api {
        @POST(RUTA_ADD)
        Call<Void> insertarComentario(@Body Comentario comentario);

        @GET(RUTA_GET)
        Call<ListaComentarios> obtenerComentarios(@Query(Data.ID_REPORTE) int idReporte);

        @POST(RUTA_DELETE)
        Call<Void> deleteComentario(@Body Comentario comentario);

        @POST(RUTA_UPDATE)
        Call<Void> updateComentario(@Body Comentario comentario);
    }
}

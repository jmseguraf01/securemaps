package com.example.securemaps.Dialog.VincularCuenta;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.securemaps.R;
import com.example.securemaps.databinding.FragmentDialogVincularCuentaBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;


public class VincularCuentaDialogFragment extends DialogFragment {
    FragmentDialogVincularCuentaBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentDialogVincularCuentaBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.enlazarCuentaGoogle.setOnClickListener(click -> {
            DynamicToast.makeSuccess(getContext(), getString(R.string.cuenta_vinculada)).show();
            dismiss();
        });
    }
}
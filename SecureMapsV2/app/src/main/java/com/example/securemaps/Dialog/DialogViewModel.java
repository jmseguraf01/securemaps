package com.example.securemaps.Dialog;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;

public class DialogViewModel extends ViewModel {

    // Variable que contiene los datos del dialogo modificar
    public MutableLiveData<DataDialogModificar> dataDialogModificar = new MutableLiveData<>();
    public MutableLiveData<DataDialogConfirmar> dataDialogConfirmar = new MutableLiveData<>();
}

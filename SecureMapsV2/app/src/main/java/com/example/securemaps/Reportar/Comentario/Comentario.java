package com.example.securemaps.Reportar.Comentario;

import com.example.securemaps.Api.Data;
import com.google.gson.annotations.SerializedName;

public class Comentario {
    @SerializedName(Data.ID)
    public int id;
    @SerializedName(Data.ID_REPORTE)
    public int idReporte;
    @SerializedName(Data.ID_USUARIO)
    public int idUsuario;
    @SerializedName(Data.COMENTARIO)
    public String comentario;

    public Comentario(int idReporte, int idUsuario, String comentario) {
        this.idReporte = idReporte;
        this.idUsuario = idUsuario;
        this.comentario = comentario;
    }

    public Comentario(int id) {
        this.id = id;
    }
}

package com.example.securemaps.Login;

import com.example.securemaps.Api.Data;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName(Data.ID)
    public int id;
    @SerializedName(Data.NUEVO_USUARIO)
    public String username;
    @SerializedName(Data.NUEVO_EMAIL)
    public String email;
    @SerializedName(Data.NUEVO_PASSWORD)
    public String password;
    @SerializedName(Data.ID_USUARIO_GOOGLE)
    public String idUsuarioGoogle;
    @SerializedName(Data.NOTIFICAR_REPORTE)
    public boolean notificarReporte;


    public User() {}

    public User(String username, String email, String password, boolean notificarReporte) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.notificarReporte = notificarReporte;
    }

    public User(String username, String email, String password, String idUsuarioGoogle, boolean notificarReporte) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.idUsuarioGoogle = idUsuarioGoogle;
        this.notificarReporte = notificarReporte;
    }

    public User(int id, boolean notificarReporte) {
        this.id = id;
        this.notificarReporte = notificarReporte;
    }

    // Constructor para recuperar password
    public User(String email) {
        this.email = email;
    }
}

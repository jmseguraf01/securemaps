package com.example.securemaps.Api;

public class Data {
    public static final String URL = "https://api.securemaps.es:8444/";
    public static final String CAMPO_EMAIL = "email";
    public static final String CAMPO_PASSWORD = "password";

    public static final String ID = "id";
    public static final String ID_USUARIO = "id_usuario";
    public static final String CALLE = "calle";
    public static final String CIUDAD = "ciudad";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String COMENTARIO = "comentario";
    public static final String LUGAR = "lugar";
    public static final String NUEVO_USUARIO = "nuevo_usuario";
    public static final String NUEVO_EMAIL = "nuevo_email";
    public static final String NUEVO_PASSWORD = "nuevo_password";
    public static final String ANONIMO = "anonimo";
    public static final String ID_USUARIO_GOOGLE = "id_google";
    public static final String ID_REPORTE = "id_reporte";
    public static final String NOTIFICAR_REPORTE = "notificar_reporte";
}

package com.example.securemaps.Ranking;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.securemaps.Api.ApiReportes;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Gestionar.Reportes.GestionarReportesFragment;
import com.example.securemaps.R;
import com.example.securemaps.Reportar.MapaPrincipalFragment;
import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Reportar.ReporteViewModel;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.Utils.SMViewModel;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentRankingBinding;
import com.example.securemaps.databinding.ViewholderRankingCiudadesBinding;
import com.example.securemaps.databinding.ViewholderReportesBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RankingFragment extends BaseFragment {
    private FragmentRankingBinding binding;
    private CiudadesAdapter ciudadesAdapter;
    private ReporteViewModel reporteViewModel;
    private List<Reporte> reporteList = new ArrayList<>();
    private HashMap<String, Integer> reportesMap = new HashMap<>();;
    private FiltroRankingFragment filtroRankingFragment;

    public enum Orden {
        ASCENDIENTE, DESCENDIENTE;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentRankingBinding.inflate(inflater, container, false)).getRoot();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        obtenerReportes();

        // Click en el boton del filtro
        binding.filter.setOnClickListener(click ->  {
                filtroRankingFragment = new FiltroRankingFragment();
                filtroRankingFragment.show(getParentFragmentManager(), "");
        });

        // Si los datos del orden cambia lo actualizo
        SMViewModel.ordenRankingReportes.observe(getViewLifecycleOwner(), new Observer<Orden>() {
            @Override
            public void onChanged(Orden orden) {
                // Actualizo los datos con orden ascendente
                if (orden == Orden.ASCENDIENTE) {
                    actualizarDatos(ordenarAscendente());
                }
                // Actualizo los datos con orden descendente
                else if (orden == Orden.DESCENDIENTE) {
                    actualizarDatos(ordenarDescendente());
                }
            }
        });
    }

    // RecyclerView de las ciudades
    class CiudadesAdapter extends RecyclerView.Adapter<RankingCiudadesViewHolder> {
        private List<String> ciudadList;
        private List<Integer> ciudadesCount = new ArrayList<>();
        private ReportesAdapter reportesAdapter;
        private boolean ocultar = true;

        @NonNull
        @NotNull
        @Override
        public RankingCiudadesViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new RankingCiudadesViewHolder(ViewholderRankingCiudadesBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RankingCiudadesViewHolder holder, int position) {
            String ciudad = ciudadList.get(position);
            holder.binding.textViewCiudad.setTextSize(15);
            holder.binding.textViewCiudad.setText(ciudad);
            holder.binding.textViewNumeroReportes.setText(String.valueOf(ciudadesCount.get(position)) + " Reportes");
            holder.binding.imageViewUbicacion.setOnClickListener(click -> {
                mostrarUbicacion(ciudad);
            });

            // Reseteo la lista de reportes de esta ciudad
            reportesAdapter = new ReportesAdapter();
            reportesAdapter.setReporteList(new ArrayList<>());
            holder.binding.listaReportes.setAdapter(reportesAdapter);

            // Mostrar los reportes de una ciudad
            holder.binding.imageViewDesplegar.setOnClickListener(click -> {
                verReportes(holder, ciudad);
            });

            // Ocultar reportes de una ciudad
            holder.binding.imageViewOcultar.setOnClickListener(click -> {
                ocultarReportes(holder);
            });

            // Click en el layout
            holder.binding.linearGeneralCiudad.setOnClickListener(click -> {
                if (ocultar) {
                    verReportes(holder, ciudad);
                    ocultar = false;
                } else {
                    ocultarReportes(holder);
                    ocultar = true;
                }
            });

        }

        // Muestra el recycler view de los reportes de una ciudad
        private void verReportes(RankingCiudadesViewHolder holder, String ciudad) {
            // Cambio los botones
            holder.binding.imageViewDesplegar.setVisibility(View.GONE);
            holder.binding.imageViewOcultar.setVisibility(View.VISIBLE);
            // Creo el adaptador y actualizo la lista
            reportesAdapter = new ReportesAdapter();
            mostrarReportes(ciudad, reportesAdapter);
            holder.binding.listaReportes.setAdapter(reportesAdapter);
            holder.binding.listaReportes.setVisibility(View.VISIBLE);
        }

        // Oculta el recycler view de los reportes de una ciudad
        private void ocultarReportes(RankingCiudadesViewHolder holder) {
            // Cambio los botones
            holder.binding.listaReportes.setVisibility(View.GONE);
            holder.binding.imageViewOcultar.setVisibility(View.GONE);
            holder.binding.imageViewDesplegar.setVisibility(View.VISIBLE);
        }

        @Override
        public int getItemCount() {
            if (ciudadList == null) {
                return 0;
            } else {
                return ciudadList.size();
            }
        }

        public void setCiudadList(List<String> ciudadList) {
            this.ciudadList = ciudadList;
            notifyDataSetChanged();
        }

        public void setCiudadesCount(List<Integer> ciudadesCount) {
            this.ciudadesCount = ciudadesCount;
        }
    }

    // Clase del view holder de las ciudades
    class RankingCiudadesViewHolder extends RecyclerView.ViewHolder {

        ViewholderRankingCiudadesBinding binding;

        public RankingCiudadesViewHolder(ViewholderRankingCiudadesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

    // Recycler view de los reportes
    class ReportesAdapter extends RecyclerView.Adapter<GestionarReportesFragment.ReportesViewHolder> {
        private List<Reporte> reporteList = new ArrayList<>();

        @NonNull
        @NotNull
        @Override
        public GestionarReportesFragment.ReportesViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new GestionarReportesFragment.ReportesViewHolder(ViewholderReportesBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull GestionarReportesFragment.ReportesViewHolder holder, int position) {
            Reporte reporte = reporteList.get(position);
            // Oculto los botones de las acciones
            holder.binding.editar.setVisibility(View.GONE);
            holder.binding.eliminar.setVisibility(View.GONE);
            holder.binding.verComentarios.setVisibility(View.GONE);
            // Muestro la calle en el reporte
            holder.binding.texto.setText(reporte.calle);
            int colorText = holder.binding.texto.getContext().getResources().getColor(R.color.verdePrincipal);
            holder.binding.texto.setTextColor(colorText);
        }

        @Override
        public int getItemCount() {
            if (reporteList == null) {
                return 0;
            } else {
                return reporteList.size();
            }
        }

        public void setReporteList(List<Reporte> reporteList) {
            this.reporteList = reporteList;
            notifyDataSetChanged();
        }
    }

    // Obtiene todos los reportes
    private void obtenerReportes() {
        ciudadesAdapter = new CiudadesAdapter();
        binding.listaCiudades.setAdapter(ciudadesAdapter);

        ApiReportes.api.obtenerTodosReportes().enqueue(new Callback<ApiReportes.Reportes>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<ApiReportes.Reportes> call, Response<ApiReportes.Reportes> response) {
                eliminarReportesRepetidos(response.body().results);
            }

            @Override
            public void onFailure(Call<ApiReportes.Reportes> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Elimina los reportes repetidos
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void eliminarReportesRepetidos(List<Reporte> reportes) {
        // Inicializo el hasmap
        for (Reporte reporte : reportes) {
            reportesMap.put(reporte.ciudad, 0);
        }
        // Aumento 1 en el hasmap del numero de los reportes
        for (Reporte reporte : reportes) {
            reportesMap.put(reporte.ciudad, reportesMap.get(reporte.ciudad) + 1);
        }

        Map<String, Integer> listaOrdenada = new HashMap<>();
        // Orden ascendente
        if (SMViewModel.ordenRankingReportes.getValue() == Orden.ASCENDIENTE) {
            listaOrdenada = ordenarAscendente();
        }
        // Orden descendente
        else if (SMViewModel.ordenRankingReportes.getValue() == Orden.DESCENDIENTE) {
            listaOrdenada = ordenarDescendente();
        }

        actualizarDatos(listaOrdenada);
    }

    // Muestra el mapa con la ciudad indicada
    private void mostrarUbicacion(String ciudad) {
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.ALL_GONE);
        MapaPrincipalFragment mapaPrincipalFragment = new MapaPrincipalFragment();
        mapaPrincipalFragment.ciudadMostrar = ciudad;
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
    }

    // Ordena la lista con orden ascendente
    @RequiresApi(api = Build.VERSION_CODES.N)
    private LinkedHashMap<String, Integer> ordenarAscendente() {
        LinkedHashMap<String, Integer> linkedList = new LinkedHashMap<>();
        reportesMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> linkedList.put(x.getKey(), x.getValue()));
        return linkedList;
    }

    // Ordena la lista con orden descendente
    @RequiresApi(api = Build.VERSION_CODES.N)
    private Map<String, Integer> ordenarDescendente() {
        Map<String, Integer> sortedMap = reportesMap.entrySet().stream()
                .sorted(Comparator.comparingInt(e -> e.getValue()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> { throw new AssertionError(); },
                        LinkedHashMap::new
                ));
        return sortedMap;
    }

    // Actualiza los datos del adaptador
    private void actualizarDatos(Map<String, Integer> listaOrdenada) {
        // Envio al adaptador las listas
        ciudadesAdapter.setCiudadesCount(new ArrayList<Integer>(listaOrdenada.values()));
        ciudadesAdapter.setCiudadList(new ArrayList<String>(listaOrdenada.keySet()));
    }


    // Muestra los reportes de una ciudad
    private void mostrarReportes(String ciudad, ReportesAdapter reportesAdapter) {
        // Obtiene los reportes de la calle
        ApiReportes.api.obtenerReportesPorCiudad(ciudad).enqueue(new Callback<ApiReportes.Reportes>() {
            @Override
            public void onResponse(Call<ApiReportes.Reportes> call, Response<ApiReportes.Reportes> response) {
                reportesAdapter.setReporteList(response.body().results);
            }

            @Override
            public void onFailure(Call<ApiReportes.Reportes> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }
}
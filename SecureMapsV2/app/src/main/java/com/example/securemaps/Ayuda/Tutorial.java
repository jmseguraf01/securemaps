package com.example.securemaps.Ayuda;

import android.view.View;

import com.example.securemaps.MainActivity;
import com.example.securemaps.R;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

public class Tutorial {
    private MainActivity ma;
    private TapTarget tapTarget;

    // Constructor
    public Tutorial(MainActivity ma) {
        this.ma = ma;
    }

    public void mapaPrincipal() {
        new TapTargetSequence(ma).targets(
                TapTarget.forView(ma.findViewById(R.id.floating_button_crear_reporte), ma.getString(R.string.crear_reporte_titulo), ma.getString(R.string.crear_reporte_tutorial)).cancelable(false).transparentTarget(true),
                TapTarget.forView(ma.findViewById(R.id.floatingButtonUbicacion), ma.getString(R.string.obtener_ubicacion), ma.getString(R.string.obtener_ubicacion_descripcion)).cancelable(false).transparentTarget(true),
                TapTarget.forView(ma.findViewById(R.id.editTextBuscar), ma.getString(R.string.buscar), ma.getString(R.string.buscar_descripcion)).targetRadius(180).cancelable(false).transparentTarget(true))
                .start();
    }

    public void gestionarReportes(int opcion) {
        // Muestra el tutorial de "todos los lugares"
        if (opcion == 0) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.ver_comentarios), ma.getString(R.string.comentarios), ma.getString(R.string.comentarios_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.editar), ma.getString(R.string.modificar_reporte_titulo), ma.getString(R.string.modificar_reporte_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.eliminar), ma.getString(R.string.eliminar_reporte_titulo), ma.getString(R.string.eliminar_reporte_tutorial)).cancelable(false).transparentTarget(true)
            ).start();
        }
        // Muestra el tutorial para buscar lugares
        else if (opcion == 1) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.editTextBuscarReporte), ma.getString(R.string.buscar_reportes_titulo), ma.getString(R.string.buscar_reportes_tutorial)).cancelable(false).transparentTarget(true).targetRadius(180)
            ).start();
        }
    }

    public void lugaresEtiquetados() {
        // Anyadir lugar visible
        if (ma.findViewById(R.id.anyadirLugar).getVisibility() == View.VISIBLE) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.lugar), ma.getString(R.string.lugar), ma.getString(R.string.lugar_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.anyadirLugar), ma.getString(R.string.anyadir_lugar_titulo), ma.getString(R.string.anyadir_lugar_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.eliminar_lugar), ma.getString(R.string.eliminar_lugar_titulo), ma.getString(R.string.eliminar_lugar_tutorial)).cancelable(false).transparentTarget(true)
            ).start();
        }
        // Editar lugar visible
        else if (ma.findViewById(R.id.anyadirLugar).getVisibility() == View.GONE) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.lugar), ma.getString(R.string.lugar), ma.getString(R.string.lugar_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.editarLugar), ma.getString(R.string.modificar_lugar_titulo), ma.getString(R.string.modificar_lugar_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.eliminar_lugar), ma.getString(R.string.eliminar_lugar_titulo), ma.getString(R.string.eliminar_lugar_tutorial)).cancelable(false).transparentTarget(true)
            ).start();
        }
    }

    public void lugaresFavoritos(int opcion) {
        // No hay ningun lugar favorito, solo muestra el boton de crear un lugar
        if (opcion == 1) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.crearLugarFavorito), ma.getString(R.string.crear_favorito_titulo), ma.getString(R.string.crear_favorito_tutorial)).cancelable(false).transparentTarget(true)
            ).start();
        }

        // Hay lugares favoritos creados
        else if (opcion == 0) {
            new TapTargetSequence(ma).targets(
                    TapTarget.forView(ma.findViewById(R.id.mostrarFavorito), ma.getString(R.string.mostrar_favorito_titulo), ma.getString(R.string.mostrar_favorito_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.modificarLugar), ma.getString(R.string.modificar_lugar_favorito_titulo), ma.getString(R.string.modificar_lugar_favorito_tutorial)).cancelable(false).transparentTarget(true),
                    TapTarget.forView(ma.findViewById(R.id.eliminarFavorito), ma.getString(R.string.eliminar_favorito_titulo), ma.getString(R.string.eliminar_favorito_tutorial)).cancelable(false).transparentTarget(true)
            ).start();
        }
    }

    public void configuracion() {
//        if (ma.findViewById(R.id.buttonVerPassword).getVisibility() == View.VISIBLE) {
//            new TapTargetSequence(ma).targets(
//                    TapTarget.forView(ma.findViewById(R.id.textViewUsuario), ma.getString(R.string.usuario_titulo), ma.getString(R.string.usuario_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonCambiarUsuario), ma.getString(R.string.modificar_usuario_titulo), ma.getString(R.string.modificar_usuario_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.textViewPassword), ma.getString(R.string.password_titulo), ma.getString(R.string.password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonVerPassword), ma.getString(R.string.mostrar_password), ma.getString(R.string.mostrar_password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonCambiarPassword), ma.getString(R.string.modificar_password_titulo), ma.getString(R.string.modificar_password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonVincularGoogle), ma.getString(R.string.enlazar_cuenta_titulo), ma.getString(R.string.enlazar_cuenta_tutorial)).cancelable(false).transparentTarget(true).targetRadius(180),
//                    TapTarget.forView(ma.findViewById(R.id.switchNotificarIncidentes), ma.getString(R.string.notificar_incidentes_titulo), ma.getString(R.string.notificar_incidentes_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonEliminarCuenta), ma.getString(R.string.eliminar_cuenta), ma.getString(R.string.eliminar_cuenta_tutorial)).cancelable(false).transparentTarget(true).targetRadius(180)
//            ).start();
//        } else if (ma.findViewById(R.id.buttonOcultarPassword).getVisibility() == View.VISIBLE) {
//            new TapTargetSequence(ma).targets(
//                    TapTarget.forView(ma.findViewById(R.id.textViewUsuario), ma.getString(R.string.usuario_titulo), ma.getString(R.string.usuario_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonCambiarUsuario), ma.getString(R.string.modificar_usuario_titulo), ma.getString(R.string.modificar_usuario_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.textViewPassword), ma.getString(R.string.password_titulo), ma.getString(R.string.password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonOcultarPassword), ma.getString(R.string.ocultar_password), ma.getString(R.string.ocultar_password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonCambiarPassword), ma.getString(R.string.modificar_password_titulo), ma.getString(R.string.modificar_password_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonVincularGoogle), ma.getString(R.string.enlazar_cuenta_titulo), ma.getString(R.string.enlazar_cuenta_tutorial)).cancelable(false).transparentTarget(true).targetRadius(180),
//                    TapTarget.forView(ma.findViewById(R.id.switchNotificarIncidentes), ma.getString(R.string.notificar_incidentes_titulo), ma.getString(R.string.notificar_incidentes_tutorial)).cancelable(false).transparentTarget(true),
//                    TapTarget.forView(ma.findViewById(R.id.buttonEliminarCuenta), ma.getString(R.string.eliminar_cuenta), ma.getString(R.string.eliminar_cuenta_tutorial)).cancelable(false).transparentTarget(true).targetRadius(180)
//            ).start();

//        }

    }
}

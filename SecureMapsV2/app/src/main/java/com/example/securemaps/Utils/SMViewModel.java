package com.example.securemaps.Utils;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.securemaps.Ranking.RankingFragment;
import com.example.securemaps.Reportar.Comentario.Comentario;
import com.example.securemaps.Reportar.Reporte;

public class SMViewModel extends ViewModel {
    public MutableLiveData<String> codeRegistro = new MutableLiveData<>();
    public MutableLiveData<Integer> codeLogin = new MutableLiveData<>();
    public Reporte reporteComentario;
    public MutableLiveData<byte[]> imagenUsuario = new MutableLiveData<>();
    public static Comentario comentarioOpciones;
    public static MutableLiveData<Boolean> actualizarComentarios = new MutableLiveData<>();
    public MutableLiveData<Boolean> actualizarImagen = new MutableLiveData<>();
    public static MutableLiveData<RankingFragment.Orden> ordenRankingReportes = new MutableLiveData<>(RankingFragment.Orden.ASCENDIENTE);
}



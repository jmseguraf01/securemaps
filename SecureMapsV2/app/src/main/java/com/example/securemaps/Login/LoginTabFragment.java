package com.example.securemaps.Login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.securemaps.Api.ApiLogin;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.R;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.databinding.LoginTabFragmentBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginTabFragment extends BaseFragment {
    private LoginTabFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = LoginTabFragmentBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Configuracion del layout
        binding.usuario.setTranslationX(800);
        binding.password.setTranslationX(800);
        binding.forgetPass.setTranslationX(800);
        binding.buttonLogin.setTranslationX(800);

        binding.usuario.setAlpha(0);
        binding.password.setAlpha(0);
        binding.forgetPass.setAlpha(0);
        binding.buttonLogin.setAlpha(0);

        binding.usuario.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        binding.password.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        binding.forgetPass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        binding.buttonLogin.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();

        // Click button login
        binding.buttonLogin.setOnClickListener(click -> {
            String usuario = binding.usuario.getText().toString();
            String password = binding.password.getText().toString();
            // Siempre que los campos esten rellenados
            if (!usuario.equals("") && !password.equals("")) {
                // Si el campo de usuario tiene un formato de email
                if (utils.validarEmail(usuario)) {
                    login(usuario, password);
                } else {
                    binding.usuario.setError(getString(R.string.campo_formato_email));
                }
            }
            // Si hay campos en blanco, los marco
            else {
                if (usuario.equals("")) {
                    binding.usuario.setError(getString(R.string.campo_obligatorio));
                }
                if (password.equals("")) {
                    binding.password.setError(getString(R.string.campo_obligatorio));
                }
            }
        });

        inicioSesionAutomatico();

        // Cuando se hace login
        smViewModel.codeLogin.observe(getViewLifecycleOwner(), code -> {
            // Login con exito
            if (code == 200) {
                navController.navigate(R.id.go_to_MapaPrincipalFragment);
            }
        });

        // Click boton recuperar contraseña
        binding.forgetPass.setOnClickListener(click -> {
            recuperarContrasenya();
        });
    }

    // Intenta iniciar sesion, si ya se ha logueado anteriormente un usuario
    private void inicioSesionAutomatico() {
        String email = utils.obtenerEmail();
        String password = utils.obtenerPassword();
        // Si tiene el usuario y password guardada
        if (email != "" && password != "") {
            login(email, password);
        }
    }

    // Metodo que hace login
    public void login(String email, String password) {
        ApiLogin.api.buscar(email, password).enqueue(new Callback<ApiLogin.Respuesta>() {
            @Override
            public void onResponse(@NonNull Call<ApiLogin.Respuesta> call, @NonNull Response<ApiLogin.Respuesta> response) {
                // Login correcto
                if (response.code() == 200) {
                    utils.guardarUsuario(response.body().id, response.body().usuario, response.body().email, response.body().password, response.body().notificarReporte);
                    smViewModel.actualizarImagen.setValue(true);
                    navController.navigate(R.id.go_to_MapaPrincipalFragment);
                }
                // Login incorrecto
                else if (response.code() == 401) {
                    DynamicToast.makeError(getContext(), getContext().getString(R.string.usuario_incorrecto)).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiLogin.Respuesta> call, @NonNull Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Funcion para recuperar la contraseña perdida
    private void recuperarContrasenya() {
        String email = binding.usuario.getText().toString();
        // Siempre que haya escrito un email
        if (!email.isEmpty() && utils.validarEmail(email)) {
            ApiLogin.api.recuperarPassword(new User(email)).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    // Email enviado conn exito
                    if (response.code() == 200) {
                        DynamicToast.makeSuccess(getContext(), getString(R.string.email_enviado)).show();
                    }
                    // Email no existe en la base de datos
                    else if (response.code() == 404) {
                        DynamicToast.makeError(getContext(), getString(R.string.email_no_existe)).show();
                    }
                    // Error al enviar email
                    else {
                        DynamicToast.makeError(getContext(), getString(R.string.error_enviar_mail)).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    ErrorCode.errorApi(getContext(), t.getMessage());
                }
            });
        }
        // Email vacio
        else if (email.isEmpty()) {
            binding.usuario.setError(getString(R.string.campo_obligatorio));
        }
        // Formato de email incorrecto
        else if (!utils.validarEmail(email)) {
            binding.usuario.setError(getString(R.string.campo_formato_email));
        }


    }
}

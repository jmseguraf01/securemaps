package com.example.securemaps.Configuracion;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.example.securemaps.Api.ApiUsuario;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;
import com.example.securemaps.Dialog.DialogModificar.DialogModificarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.Login.User;
import com.example.securemaps.R;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentConfiguracionBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfiguracionFragment extends BaseFragment {
    private FragmentConfiguracionBinding binding;
    private DialogViewModel dialogViewModel;
    private DialogModificarFragment dialogModificarFragment;
    private DialogConfirmarFragment dialogConfirmarFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentConfiguracionBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        // Lo marco como clickado
        binding.switchNotificarIncidentes.setChecked(true);
        // Oculto el password
        binding.textViewPassword.setTransformationMethod(new PasswordTransformationMethod());

        // Actualizo los datos
        actualizarDatos();

        // Cambiar usuario
        binding.buttonCambiarUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarUsuario();
            }
        });

        // Cambiar correo
        binding.buttonCambiarEmail.setOnClickListener(click -> {
            // Siempre que no este logueado con google
            if (utils.obtenerIdGoogle() == null) {
                cambiarEmail();
            } else {
                DynamicToast.makeError(getContext(), getString(R.string.no_cambio_email_google)).show();
            }
        });

        // Click boton cambiar password
        binding.buttonCambiarPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarPassword();
            }
        });

        // Click en el boton de informacion
        binding.informacionNotificarIncidente.setOnClickListener(click -> {
            utils.alertDialog(getActivity(), getString(R.string.notificar_incidentes_titulo), getString(R.string.notificar_incidentes_tutorial), getString(R.string.boton_aceptar)).show();
        });

        // Cuando se le da click al switch de notificar reporte
        binding.switchNotificarIncidentes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                clickCheck(isChecked);
            }
        });

        // Click en el boton editar correo electronico
        binding.editarCorreoElectronico.setOnClickListener(click -> {
            // Siempre que no este logueado con google
            if (utils.obtenerIdGoogle() == null) {
                cambiarEmail();
            } else {
                DynamicToast.makeError(getContext(), getString(R.string.no_cambio_email_google)).show();
            }
        });

        // Click en el boton borrar cuenta
        binding.buttonEliminarCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarCuenta();
            }
        });
    }

    // Muestra los datos en los edittext
    private void actualizarDatos() {
        binding.textViewUsuario.setText(utils.obtenerUsuario());
        binding.textViewEmail.setText(utils.obtenerEmail());
        binding.textViewEmailIncidentes.setText(utils.obtenerEmail());
        if (utils.obtenerNotificacionReportes()) {
            binding.switchNotificarIncidentes.setChecked(true);
            binding.layoutDireccionCorreo.setVisibility(View.VISIBLE);
        } else if (!utils.obtenerNotificacionReportes()) {
            binding.switchNotificarIncidentes.setChecked(false);
            binding.layoutDireccionCorreo.setVisibility(View.GONE);
        }
    }

    // Cambiar usuario
    private void cambiarUsuario() {
        // Transicion del dialogo
        FragmentTransaction ft = new Utils().fragmentTransaction(this);
        // Creo el alert dialog
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            // Click boton positivo
            @Override
            public void clickButtonPositivo(String usuario) {
                if (!usuario.equals("")) {
                    // Actualizo el usuario
                    ApiUsuario.api.updateUser(new User(usuario, utils.obtenerEmail(), utils.obtenerPassword(), utils.obtenerNotificacionReportes())).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            // Actualizado con exito
                            if (response.code() == 200) {
                                DynamicToast.makeSuccess(getContext(), getString(R.string.usuario_actualizado), Toast.LENGTH_LONG).show();
                                utils.guardarUsuario(utils.obtenerIdUsuario(), usuario, utils.obtenerEmail(), utils.obtenerPassword(), utils.obtenerNotificacionReportes());
                                System.out.println(utils.obtenerPassword());
                                actualizarDatos();
                            }
                            // Error al actualizar
                            else if (response.code() == 400) {
                                DynamicToast.makeError(getContext(), getString(R.string.error_actualizar_usuario), Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ErrorCode.errorApi(getContext(), t.getMessage());
                        }
                    });
                    dialogModificarFragment.dismiss();
                }
            }

        });

        dialogModificarFragment.show(ft, "dialog");
        // Los datos del dialog
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.modificar_usuario_titulo), getString(R.string.modificar_usuario), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);

    }

    // Cambiar email
    private void cambiarEmail() {
        FragmentTransaction ft = new Utils().fragmentTransaction(this);
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            @Override
            public void clickButtonPositivo(String email) {
                if (!email.isEmpty()) {
                    // Actualizo el correo
                    ApiUsuario.api.updateUser(new User(utils.obtenerUsuario(), email, utils.obtenerPassword(), utils.obtenerNotificacionReportes())).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.code() == 200) {
                                DynamicToast.makeSuccess(getContext(), getString(R.string.email_actualizado), Toast.LENGTH_LONG).show();
                                utils.guardarUsuario(utils.obtenerIdUsuario(), utils.obtenerUsuario(), email, utils.obtenerPassword(), utils.obtenerNotificacionReportes());
                                actualizarDatos();
                            } else if (response.code() == 400) {
                                DynamicToast.makeError(getContext(), getString(R.string.error_actualizar_email), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ErrorCode.errorApi(getContext(), t.getMessage());
                        }
                    });
                    dialogModificarFragment.dismiss();
                }
            }
        });
        // Los datos del dialog
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.modificar_email_titulo), getString(R.string.modificar_email), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
        dialogModificarFragment.show(ft, "dialog");
    }

    // Cambiar contraseña
    private void cambiarPassword() {
        // Transicion del dialogo
        FragmentTransaction ft = new Utils().fragmentTransaction(this);
        // Creo el alert dialog
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            // Click boton positivo
            @Override
            public void clickButtonPositivo(String password) {
                if (!password.equals("")) {
                    // Actualizo la contrseña
                    ApiUsuario.api.updateUserPassword(new User(null, null, password, utils.obtenerNotificacionReportes())).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.code() == 200) {
                                DynamicToast.makeSuccess(getContext(), getString(R.string.password_actualizado), Toast.LENGTH_LONG).show();
                                utils.guardarUsuario(utils.obtenerIdUsuario(), utils.obtenerUsuario(), utils.obtenerEmail(), password, utils.obtenerNotificacionReportes());
                                actualizarDatos();
                            } else if (response.code() == 400) {
                                DynamicToast.makeError(getContext(), getString(R.string.error_actualizar_password), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ErrorCode.errorApi(getContext(), t.getMessage());
                        }
                    });
                    dialogModificarFragment.dismiss();
                }
            }
        });

        dialogModificarFragment.show(ft, "dialog");
        // Los datos del dialog
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.modificar_password_titulo), getString(R.string.modificar_password), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
    }

    // Click en el check de enviar correo
    private void clickCheck(boolean isChecked) {
        // Actualizo el campo en la base de datos
        ApiUsuario.api.updateNotifacionReporte(new User(Integer.parseInt(utils.obtenerIdUsuario()), isChecked)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    DynamicToast.makeSuccess(getContext(), getString(R.string.notificar_incidentes_actualizado)).show();
                    // Lo guardo en local
                    utils.guardarNotificacionReporte(isChecked);
                } else {
                    DynamicToast.makeSuccess(getContext(), getString(R.string.notificar_incidentes_error)).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
        // Si esta clickado nuestro el linear
        if (isChecked) {
            binding.layoutDireccionCorreo.setVisibility(View.VISIBLE);
        }
        // Si no esta clickado, pongo invisible el linear
        else if (!isChecked) {
            binding.layoutDireccionCorreo.setVisibility(View.GONE);
        }
    }

    // Metodo para eliminar cuenta
    private void eliminarCuenta() {
        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                // Elimino la cuenta
                ApiUsuario.api.deleteAccount().enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Cuenta eliminada con exito
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(requireContext(), getString(R.string.usuario_eliminado), Toast.LENGTH_LONG).show();
                            utils.eliminarDatosUsuario();
                            navController.navigate(R.id.loginFragment);
                        }
                        // Error al eliminar la cuenta
                        else if (response.code() == 400) {
                            DynamicToast.makeError(requireContext(), getString(R.string.error_eliminar_usuario), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogConfirmarFragment.dismiss();
            }
        });
        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar_cuenta), getString(R.string.texto_eliminar_cuenta), getString(R.string.eliminar));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setIcon(R.drawable.ic_baseline_delete_24);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);
        dataDialogConfirmar.setColorBackgroundPositivo(requireContext().getDrawable(R.drawable.button_eliminar_positivo));

        dataDialogConfirmar.setColorBotonNegativo(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundNegativo(getContext().getDrawable(R.drawable.button_eliminar_negativo));
        dialogConfirmarFragment.show(getChildFragmentManager(), "Dialog");
    }
}
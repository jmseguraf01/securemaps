package com.example.securemaps.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.MutableLiveData;

import com.example.securemaps.Api.ApiLugaresEtiquetados;
import com.example.securemaps.Api.ApiReportes;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Configuracion.ConfiguracionFragment;
import com.example.securemaps.MainActivity;
import com.example.securemaps.R;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Utils {
    private final String ID_USUARIO ="id";
    private final String USUARIO = "usuario";
    private final String EMAIL = "email";
    private final String PASSWORD = "password";
    private static final String ID_GOOGLE = "id_google";
    private final String NOTIFICACION_REPORTES = "notificacion";
    public static Context context;

    public MutableLiveData<String> mutableLiveDataUsuario = new MutableLiveData<>();

    // Notifico los incidentes
    public void notificarIncidentes() {
    }

    // Visibilidad del boton de la pantalla principal (reportes)
    public enum ButtonVisibility {
        REPORTAR, MODIFICAR, FAVORITO, ALL_GONE;
    }
    // Devuelve un alert dialog basico

    public AlertDialog.Builder alertDialog(FragmentActivity activity, String titulo, String texto, String boton) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(texto);
        alertDialog.setNegativeButton(boton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Click en este boton no se hace nada
            }
        });
        return alertDialog;
    }

    // Retorna la transicion del dialog modificar
    public FragmentTransaction fragmentTransaction(ConfiguracionFragment configuracionFragment) {
        FragmentTransaction ft = configuracionFragment.getFragmentManager().beginTransaction();
        Fragment prev = configuracionFragment.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        return ft;
    }

    // Solicita los permisos necesarios para iniciar la app
    public void solicitarPermisos(Context context, FragmentActivity activity) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
        }
    }

    // Funcion que obtiene el id del usuario
    public String obtenerIdUsuario() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getString(ID_USUARIO, "");
    }

    // Obtiene el nombre de usuario
    public String obtenerUsuario() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getString(USUARIO, "");
    }

    // Funcion que obtiene el correo de la sesion actual
    public String obtenerEmail() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getString(EMAIL, "");
    }

    // Funcion que obtiene la password
    public String obtenerPassword() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getString(PASSWORD, "");
    }

    // Obtiene el id de google
    public String obtenerIdGoogle() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getString(ID_GOOGLE, "");
    }

    // Obtiene la notificacion de los reportes
    public boolean obtenerNotificacionReportes() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        return prefs.getBoolean(NOTIFICACION_REPORTES, true);
    }

    // Elimina los datos guardados del usuario, excepto el password de google
    public void eliminarDatosUsuario() {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        prefs.edit().clear().apply();
    }

    // Guarda usuario y contraseña
    public void guardarUsuario(String id, String usuario, String email, String password, boolean notificarReporte) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        prefs.edit().putString(ID_USUARIO, id).apply();
        prefs.edit().putString(USUARIO, usuario).apply();
        prefs.edit().putString(EMAIL, email).apply();
        prefs.edit().putString(PASSWORD, password).apply();
        prefs.edit().putBoolean(NOTIFICACION_REPORTES, notificarReporte).apply();
        // Actualizo el nombre de usuario en el drawer
        new MainActivity().actualizarUsuarioDrawer(usuario);
    }

    // Guarda el id de google
    public void guardarIdGoogle(String idGoogle) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        prefs.edit().putString(ID_GOOGLE, idGoogle).apply();
    }

    // Guarda la notificacion del usuario
    public void guardarNotificacionReporte(boolean checked) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.package_name), Context.MODE_PRIVATE);
        prefs.edit().putBoolean(NOTIFICACION_REPORTES, checked).apply();
    }

    // Devuelve si el tutorial ya ha sido ejecutado o no
    public boolean tutorialEjecutado(Context context) {
        String fileDir = "tutorial_ejecutado.txt";
        String ruta = context.getFilesDir() + "/" + fileDir;
        File file = new File(ruta);
        // Si el fichero existe
        if (file.exists()) {
            return true;
        }
        // Si no existe el fichero, lo creo
        else {
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = context.openFileOutput(fileDir, context.MODE_PRIVATE);
                fileOutputStream.write("".getBytes());
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    // Crea los datos de los lugares por defecto en la DDBB
    public void crearLugaresEtiquetados() {
        ApiLugaresEtiquetados.api.inserts().enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 400) {
                    DynamicToast.makeError(context, context.getString(R.string.error_insertar_lugares_etiquetados)).show();
                } else if (response.code() == 200) {
                    System.out.println("Lugares etiquetados guardados.");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ErrorCode.errorApi(context, t.getMessage());
            }
        });
    }

    // Valida si el texto que le pasas es un email
    public boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    // Devuelve un string random
    public String generateString(int lenght) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";
        StringBuilder string = new StringBuilder(12);
        for (int i = 0; i < 12; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            string.append(AlphaNumericString.charAt(index));
        }
        return string.toString();
    }

    // Funcion que oculta el teclado
    public void ocultarTeclado(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}

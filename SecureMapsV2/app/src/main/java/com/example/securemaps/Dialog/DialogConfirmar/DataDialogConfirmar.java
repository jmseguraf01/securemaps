package com.example.securemaps.Dialog.DialogConfirmar;

import android.graphics.drawable.Drawable;

public class DataDialogConfirmar{
    String title;
    String text;
    String textoBotonPositivo;
    String colorBotonPositivo;
    String textoBotonNegativo;
    String colorBotonNegativo;
    String colorTitle;
    int icon;
    String colorIcon;
    Drawable colorBackgroundPositivo;
    Drawable colorBackgroundNegativo;

    public DataDialogConfirmar(String title, String text, String textoBotonPositivo) {
        this.title = title;
        this.text = text;
        this.textoBotonPositivo = textoBotonPositivo;
        this.icon = -1;
    }

    public void setColorBotonPositivo(String colorBotonPositivo) {
        this.colorBotonPositivo = colorBotonPositivo;
    }

    public void setColorTitle(String colorTitle) {
        this.colorTitle = colorTitle;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setColorIcon(String colorIcon) {
        this.colorIcon = colorIcon;
    }


    public void setColorBackgroundPositivo(Drawable colorBackgroundPositivo) {
        this.colorBackgroundPositivo = colorBackgroundPositivo;
    }

    public void setColorBotonNegativo(String colorBotonNegativo) {
        this.colorBotonNegativo = colorBotonNegativo;
    }

    public void setColorBackgroundNegativo(Drawable colorBackgroundNegativo) {
        this.colorBackgroundNegativo = colorBackgroundNegativo;
    }
}

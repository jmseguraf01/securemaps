package com.example.securemaps.Api;


import com.example.securemaps.Login.User;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ApiLogin {
    private static final String RUTA = "login";
    private static final String RUTA_WITH_GOOGLE = "loginGoogle";
    private static final String RUTA_RECUPERAR_PASSWORD = "resetpassword";

    public class Respuesta {
        public String error;
        public String id;
        public String usuario;
        public String email;
        public String password;
        public boolean notificarReporte;
    }

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(Api.class);

    public interface Api {
        @GET(RUTA)
        Call<Respuesta> buscar(@Query(Data.CAMPO_EMAIL) String email,
                               @Query(Data.CAMPO_PASSWORD) String password);

        @GET(RUTA_WITH_GOOGLE)
        Call<Respuesta> buscarGoogle(@Query(Data.CAMPO_EMAIL) String email,
                               @Query(Data.ID_USUARIO_GOOGLE) String password);

        @POST(RUTA_RECUPERAR_PASSWORD)
        Call<Void> recuperarPassword(@Body User user);
    }

}

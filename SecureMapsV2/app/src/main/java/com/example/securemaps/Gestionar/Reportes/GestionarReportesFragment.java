package com.example.securemaps.Gestionar.Reportes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.securemaps.Api.ApiReportes;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;
import com.example.securemaps.Dialog.DialogModificar.DialogModificarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.R;
import com.example.securemaps.Reportar.MapaPrincipalFragment;
import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Reportar.ReporteViewModel;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentGestionarReportesBinding;
import com.example.securemaps.databinding.ViewholderReportesBinding;
import com.google.android.gms.maps.model.Marker;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GestionarReportesFragment extends Fragment {
    FragmentGestionarReportesBinding binding;
    private DialogViewModel dialogViewModel;
    private DialogConfirmarFragment dialogConfirmarFragment;
    private MapaPrincipalFragment mapaPrincipalFragment;
    private DialogModificarFragment dialogModificarFragment;
    private Utils utils = new Utils();
    private ReporteViewModel reporteViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentGestionarReportesBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        mostrarTodosReportes();
        // Click en el switch ver todos
        binding.switchVerTodos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Esta pulsado
                if (isChecked) {
                    binding.linearBuscarReportes.setVisibility(View.GONE);
                    mostrarTodosReportes();
                    binding.listaTodosReportes.setVisibility(View.VISIBLE);
                }
                // No esta pulsado
                else {
                    binding.editTextBuscarReporte.setText("");
                    binding.listaTodosReportes.setVisibility(View.GONE);
                    binding.linearBuscarReportes.setVisibility(View.VISIBLE);
                }
            }
        });

        // Click boton buscar
        binding.buttonBuscarReporte.setOnClickListener(click -> {
            String calleBuscar = binding.editTextBuscarReporte.getText().toString().toLowerCase();
            if (!calleBuscar.equals("")) {
                // Lo pongo en este formato para que en el query busque todoo lo parecido a este string
                buscarReporte(calleBuscar);
            } else {
                new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.texto_en_blanco), getString(R.string.boton_aceptar)).show();
            }
        });
    }

    // Recyclerview
    class ReportesAdaptaer extends RecyclerView.Adapter<ReportesViewHolder> {

        private List<Reporte> reporteList;

        @NonNull
        @Override
        public ReportesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ReportesViewHolder(ViewholderReportesBinding.inflate(getLayoutInflater(), parent, false));
        }


        @Override
        public void onBindViewHolder(@NonNull ReportesViewHolder holder, int position) {
            Reporte reporte = reporteList.get(position);
            holder.binding.texto.setText(reporte.calle);
            // Ver comentarios
            holder.binding.verComentarios.setOnClickListener(click -> {
                verComentarios(reporteList.get(position));
            });
            // Editar reporte
            holder.binding.editar.setOnClickListener(click -> {
                editarReporte(reporteList.get(position));
            });
            // Eliminar reporte
            holder.binding.eliminar.setOnClickListener(click -> {
                eliminarReporte(reporteList.get(position));
            });
            System.out.println("pasa");

        }

        @Override
        public int getItemCount() {
            if (reporteList == null) {
                return 0;
            } else {
                return reporteList.size();
            }
        }

        public void setReporteList(List<Reporte> reporteList) {
            this.reporteList = reporteList;
            notifyDataSetChanged();
        }


    }

    public static class ReportesViewHolder extends RecyclerView.ViewHolder {

        public ViewholderReportesBinding binding;

        public ReportesViewHolder(ViewholderReportesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

    // Funcion que muestra los comentarios del lugar
    private void verComentarios(Reporte reporte) {
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            @Override
            public void clickButtonPositivo(String nuevoComentario) {
                // Siempre que el comentario sea diferente al que ya hay
                if (!reporte.comentario.equals(nuevoComentario)) {
                    // Actualizo el comentario
                    ApiReportes.api.updateReportes(new Reporte(reporte.id, reporte.calle, reporte.ciudad, reporte.latitude, reporte.longitude, nuevoComentario)).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            // Actualizado
                            if (response.code() == 200) {
                                DynamicToast.makeSuccess(getContext(), getString(R.string.comentario_modificado)).show();
                                mostrarTodosReportes();
                            }
                            // Error al actualizar
                            else if (response.code() == 400) {
                                DynamicToast.makeError(getContext(), getString(R.string.error_comentario_modificado)).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            System.out.println("HA PETADO");
                            System.out.println(t.getMessage());
                        }
                    });
                    dialogModificarFragment.dismiss();
                } else {
                    DynamicToast.make(getContext(), getText(R.string.comentario_no_cambiado), Toast.LENGTH_LONG).show();
                }
            }
        });

        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.comentarios), getString(R.string.texto_comentarios), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dataDialogModificar.setTextoEdittext(reporte.comentario);
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
        dialogModificarFragment.show(getFragmentManager(), "Dialog");
    }

    // Funcion para editar el reporte que le pasas
    private void editarReporte(Reporte reporte) {
        DynamicToast.make(getContext(), getString(R.string.modificar_reporte_instrucciones), Toast.LENGTH_LONG).show();
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.MODIFICAR);
        mapaPrincipalFragment = new MapaPrincipalFragment(new MapaPrincipalFragment.ClickButtonCallback() {
            // Click en el boton de editar reporte
            @Override
            public void click(Marker lugarMarcado) {
                // Siempre que se haya marcado un lugar
                if (lugarMarcado != null) {
                    String calle = mapaPrincipalFragment.obtenerCalle(lugarMarcado.getPosition());
                    String ciudad = mapaPrincipalFragment.obtenerCiudad(lugarMarcado.getPosition());
                    dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
                        @Override
                        public void clickButtonPositivo() {
                            double latitude = lugarMarcado.getPosition().latitude;
                            double longitude = lugarMarcado.getPosition().longitude;
                            ApiReportes.api.updateReportes(new Reporte(reporte.id, calle, ciudad, latitude, longitude, reporte.comentario)).enqueue(new Callback<Void>() {
                                @Override
                                public void onResponse(Call<Void> call, Response<Void> response) {
                                    // Actualizado
                                    if (response.code() == 200) {
                                        DynamicToast.makeSuccess(getContext(), getString(R.string.reporte_modificado)).show();
                                        mostrarTodosReportes();
                                    }
                                    // Error al actualizar
                                    else if (response.code() == 400) {
                                        DynamicToast.makeError(getContext(), getString(R.string.error_modificar_reporte)).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<Void> call, Throwable t) {
                                    ErrorCode.errorApi(getContext(), t.getMessage());
                                }
                            });
                            dialogConfirmarFragment.dismiss();
                            mapaPrincipalFragment.dismiss();
                        }
                    });
                    // Datos que van dentro del dialog
                    DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.modificar), getString(R.string.modificar_reporte) + "\n" + calle, getString(R.string.modificar));
                    dataDialogConfirmar.setIcon(R.drawable.ic_baseline_edit_24);
                    dataDialogConfirmar.setColorIcon(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorTitle(getString(R.color.verdePrincipal));
                    dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
                    dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

                    dialogConfirmarFragment.show(getFragmentManager(), "dialog");
                }
            }
        });
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
    }

    // Funcion para eliminar el reporte que se le pasa
    private void eliminarReporte(Reporte reporte) {
        // Creo los datos que van dentro del dialog
        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar), getString(R.string.eliminar_reporte), getString(R.string.eliminar));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
        dataDialogConfirmar.setIcon(R.drawable.ic_baseline_delete_24);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundPositivo(getContext().getDrawable(R.drawable.button_eliminar_positivo));

        dataDialogConfirmar.setColorBotonNegativo(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundNegativo(getContext().getDrawable(R.drawable.button_eliminar_negativo));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                ApiReportes.api.deleteReportes(reporte.id).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Eliminado
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(getContext(), getString(R.string.reporte_eliminado)).show();
                            mostrarTodosReportes();
                        }
                        // Error al eliminar
                        else if (response.code() == 400) {
                            DynamicToast.makeError(getContext(), getString(R.string.reporte_no_eliminado)).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogConfirmarFragment.dismiss();
            }
        });

        dialogConfirmarFragment.show(getFragmentManager(), "Dialog");
    }

    // Muestra todos los reportes en el recyclerview
    private void mostrarTodosReportes() {
        // Pongo el recyclerview
        ReportesAdaptaer reportesAdaptaer = new ReportesAdaptaer();
        binding.listaTodosReportes.setAdapter(reportesAdaptaer);
        // Lista de reportes
        obtenerReportes(reportes -> {
            reportesAdaptaer.setReporteList(reportes);
            // Siempre que hayan datos
            if (reportes.size() > 0) {
                // Si esta visible el texto de que no hay datos lo oculto
                if (binding.layoutNoHayDatos.getVisibility() == View.VISIBLE) {
                    binding.layoutNoHayDatos.setVisibility(View.GONE);
                }
            }
            // Si no hay datos
            else if (reportes.size() == 0) {
                binding.layoutNoHayDatos.setVisibility(View.VISIBLE);
            }
        });
    }

    public interface ObtenerReporteCallback {
        void call(List<Reporte> reportes);
    }

    // Funcion que devuelve todos los reportes del usuario
    public void obtenerReportes(ObtenerReporteCallback callback) {
        // Obtengo los reportes
        ApiReportes.api.obtenerReportes().enqueue(new Callback<ApiReportes.Reportes>() {
            @Override
            public void onResponse(Call<ApiReportes.Reportes> call, Response<ApiReportes.Reportes> response) {
                callback.call(response.body().results);
            }

            @Override
            public void onFailure(Call<ApiReportes.Reportes> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Busca un reporte en concreto
    private void buscarReporte(String calle) {
        ReportesAdaptaer reportesAdaptaer = new ReportesAdaptaer();
        binding.listaTodosReportes.setAdapter(reportesAdaptaer);
        // Busco los reportes que coincidan con la calle
        ApiReportes.api.obtenerReportes(calle).enqueue(new Callback<ApiReportes.Reportes>() {
            @Override
            public void onResponse(Call<ApiReportes.Reportes> call, Response<ApiReportes.Reportes> response) {
                // Si no se encuentra ningun reporte
                if (response.body().results.size() == 0) {
                    DynamicToast.make(getContext(), getString(R.string.reporte_no_encontrado), Toast.LENGTH_LONG).show();
                } else {
                    reportesAdaptaer.setReporteList(response.body().results);
                }
            }

            @Override
            public void onFailure(Call<ApiReportes.Reportes> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
        binding.listaTodosReportes.setVisibility(View.VISIBLE);
    }

}
package com.example.securemaps.Gestionar.Lugares.Etiquetados;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.securemaps.Api.ApiLugaresEtiquetados;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogConfirmar.DataDialogConfirmar;
import com.example.securemaps.Dialog.DialogConfirmar.DialogConfirmarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.Gestionar.Lugares.Favoritos.LugarFavorito;
import com.example.securemaps.R;
import com.example.securemaps.Reportar.MapaPrincipalFragment;
import com.example.securemaps.Reportar.ReporteViewModel;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentLugaresEtiquetadosBinding;
import com.example.securemaps.databinding.ViewholderLugaresBinding;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LugaresEtiquetadosFragment extends Fragment {
    private FragmentLugaresEtiquetadosBinding binding;
    public static final String[] LUGARES_DEFECTO = {"Casa", "Trabajo", "Gimnasio", "Escuela", "Iglesia"};
    private MapaPrincipalFragment mapaPrincipalFragment;
    private DialogConfirmarFragment dialogConfirmarFragment;
    private DialogViewModel dialogViewModel;
    private ReporteViewModel reporteViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return (binding = FragmentLugaresEtiquetadosBinding.inflate(inflater, container, false)).getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        mostrarDatos();
    }

    // Recyclerview
    class LugaresEtiquetadosAdapter extends RecyclerView.Adapter<LugaresEtiquetadosViewHolder> {

        private List<LugarEtiquetado> lugaresEtiquetadosList;

        @NonNull
        @Override
        public LugaresEtiquetadosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new LugaresEtiquetadosViewHolder(ViewholderLugaresBinding.inflate(getLayoutInflater(), parent, false));
        }


        @Override
        public void onBindViewHolder(@NonNull LugaresEtiquetadosViewHolder holder, int position) {
            LugarEtiquetado lugarEtiquetado = lugaresEtiquetadosList.get(position);
            holder.binding.lugar.setText(lugarEtiquetado.lugar);
            // Pongo la imagen que le corresponde a ese lugar
            holder.binding.imagenLugar.setImageDrawable(obtenerImagen(lugarEtiquetado.lugar, getContext()));
            // Si no tiene calle asignada ese lugar
            if (lugarEtiquetado.calle == null) {
                holder.binding.anyadirLugar.setVisibility(View.VISIBLE);
                holder.binding.editarLugar.setVisibility(View.GONE);
            } else {
                holder.binding.calle.setText(lugarEtiquetado.calle);
            }
            // OJO -> 0 = AGREGAR LUGAR, 1 = EDITAR LUGAR
            // Agregar lugar
            holder.binding.anyadirLugar.setOnClickListener(click -> {
                asginarLugar(lugaresEtiquetadosList.get(position), 0);
            });
            // Editar un lugar
            holder.binding.editarLugar.setOnClickListener(click -> {
                asginarLugar(lugaresEtiquetadosList.get(position), 1);
            });
            // Eliminar un lugar
            holder.binding.eliminarLugar.setOnClickListener(click -> {
                eliminarLugar(lugaresEtiquetadosList.get(position));
            });
        }

        @Override
        public int getItemCount() {
            if (lugaresEtiquetadosList == null) {
                return 0;
            } else {
                return lugaresEtiquetadosList.size();
            }
        }

        public void setLugaresEtiquetadosList(List<LugarEtiquetado> lugaresEtiquetadosList) {
            this.lugaresEtiquetadosList = lugaresEtiquetadosList;
            notifyDataSetChanged();
        }


    }

    class LugaresEtiquetadosViewHolder extends RecyclerView.ViewHolder {

        ViewholderLugaresBinding binding;

        public LugaresEtiquetadosViewHolder(ViewholderLugaresBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }


    // Muestra todos los lugares
    private void mostrarDatos() {
        LugaresEtiquetadosAdapter lugaresEtiquetadosAdapter = new LugaresEtiquetadosAdapter();
        binding.listaLugaresEtiquetados.setAdapter(lugaresEtiquetadosAdapter);
        ApiLugaresEtiquetados.api.getLugaresEtiquetados().enqueue(new Callback<ApiLugaresEtiquetados.Lugares>() {
            @Override
            public void onResponse(Call<ApiLugaresEtiquetados.Lugares> call, Response<ApiLugaresEtiquetados.Lugares> response) {
                lugaresEtiquetadosAdapter.setLugaresEtiquetadosList(response.body().results);
            }

            @Override
            public void onFailure(Call<ApiLugaresEtiquetados.Lugares> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
                System.out.println("Error: " + t.getMessage());
            }
        });
    }

    // Devuelve la imagen que le corresponde al lugar
    public Drawable obtenerImagen(String lugar, Context context) {
        Drawable drawable = null;
        if (lugar.equals(LUGARES_DEFECTO[0])) {
            drawable = context.getResources().getDrawable(R.drawable.ic_baseline_home_24);
        } else if (lugar.equals(LUGARES_DEFECTO[1])) {
            drawable = context.getResources().getDrawable(R.drawable.ic_baseline_business_center_24);
        } else if (lugar.equals(LUGARES_DEFECTO[2])) {
            drawable = context.getResources().getDrawable(R.drawable.ic_baseline_fitness_center_24);
        } else if (lugar.equals(LUGARES_DEFECTO[3])) {
            drawable = context.getResources().getDrawable(R.drawable.ic_baseline_school_24);
        } else if (lugar.equals(LUGARES_DEFECTO[4])) {
            drawable = context.getResources().getDrawable(R.drawable.ic_baseline_location_city_24);
        }
        return drawable;
    }

    // Asigna un lugar a una ubicacion / OJO -> 0 = AGREGAR LUGAR, 1 = EDITAR LUGAR
    private void asginarLugar(LugarEtiquetado lugarEtiquetado, int opcion) {
        DynamicToast.make(getContext(), getString(R.string.modificar_lugares_etiquetados_instrucciones), Toast.LENGTH_LONG).show();
        // Cambio el boton
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.MODIFICAR);
        mapaPrincipalFragment = new MapaPrincipalFragment(lugarMarcado -> {
            if (lugarMarcado != null) {
                String calle = mapaPrincipalFragment.obtenerCalle(lugarMarcado.getPosition());
                dialogConfirmarFragment = new DialogConfirmarFragment(() -> {
                    // Le pongo al lugar etiquetado los nuevos datos
                    lugarEtiquetado.latitude = lugarMarcado.getPosition().latitude;
                    lugarEtiquetado.longitude = lugarMarcado.getPosition().longitude;
                    lugarEtiquetado.calle = calle;
                    // Actualizo los datos
                    ApiLugaresEtiquetados.api.updateLugaresEtiquetados(new LugarEtiquetado(lugarEtiquetado.id, lugarEtiquetado.calle, lugarEtiquetado.latitude, lugarEtiquetado.longitude)).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            // Actualizado con exito
                            if (response.code() == 200) {
                                DynamicToast.makeSuccess(getContext(), getString(R.string.lugar_guardado)).show();
                                mostrarDatos();
                            }
                            // Error al actualizar
                            else if (response.code() == 400) {
                                DynamicToast.makeError(getContext(), getString(R.string.error_lugar_guardado)).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            ErrorCode.errorApi(getContext(), t.getMessage());
                        }
                    });

                    // Cierro los dialogos
                    dialogConfirmarFragment.dismiss();
                    mapaPrincipalFragment.dismiss();

                });

                DataDialogConfirmar dataDialogConfirmar = null;
                // Agregar lugar
                if (opcion == 0) {
                    dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.agregar), getString(R.string.agregar_texto) + calle + " como " + lugarEtiquetado.lugar + "?", getString(R.string.agregar));
                    dataDialogConfirmar.setIcon(R.drawable.ic_baseline_add_box_24);
                }
                // Editar lugar
                else if (opcion == 1) {
                    dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.modificar), getString(R.string.modificar_lugar) + calle + "?", getString(R.string.modificar));
                    dataDialogConfirmar.setIcon(R.drawable.ic_baseline_edit_24);
                }

                dataDialogConfirmar.setColorIcon(getString(R.color.verdePrincipal));
                dataDialogConfirmar.setColorTitle(getString(R.color.verdePrincipal));
                dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
                dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);

                dialogConfirmarFragment.show(getFragmentManager(), "Dialog");

            } else {
                new Utils().alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.error_seleccionar_lugar), getString(R.string.boton_aceptar)).show();
            }
        });
        mapaPrincipalFragment.show(getFragmentManager(), "Dialog");
    }

    // Elimina un lugar
    private void eliminarLugar(LugarEtiquetado lugarEtiquetado) {
        dialogConfirmarFragment = new DialogConfirmarFragment(new DialogConfirmarFragment.ClickDialogConfirmarCallback() {
            @Override
            public void clickButtonPositivo() {
                // Borro la calle y las coordenadas del lugar
                ApiLugaresEtiquetados.api.deleteLugaresEtiquetados(lugarEtiquetado.id).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Eliminado con exito
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(getContext(), getString(R.string.lugar_eliminado)).show();
                            mostrarDatos();
                        }
                        // Error al eliminar
                        else if (response.code() == 400) {
                            DynamicToast.makeError(getContext(), getString(R.string.error_eliminar_lugar)).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogConfirmarFragment.dismiss();
            }
        });

        DataDialogConfirmar dataDialogConfirmar = new DataDialogConfirmar(getString(R.string.eliminar), getString(R.string.eliminar_lugar), getString(R.string.eliminar));
        dataDialogConfirmar.setIcon(R.drawable.eliminar);
        dataDialogConfirmar.setColorIcon(getString(R.color.rojo));
        dataDialogConfirmar.setColorTitle(getString(R.color.rojo));
        dataDialogConfirmar.setColorBotonPositivo(getString(R.color.white));
        dataDialogConfirmar.setColorBackgroundPositivo(getContext().getDrawable(R.drawable.button_eliminar_positivo));

        dataDialogConfirmar.setColorBotonNegativo(getString(R.color.rojo));
        dataDialogConfirmar.setColorBackgroundNegativo(getContext().getDrawable(R.drawable.button_eliminar_negativo));
        dialogViewModel.dataDialogConfirmar.postValue(dataDialogConfirmar);
        dialogConfirmarFragment.show(getFragmentManager(), "Dialog");
    }

}
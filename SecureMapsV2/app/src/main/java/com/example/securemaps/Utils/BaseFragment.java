package com.example.securemaps.Utils;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class BaseFragment extends Fragment {
    public NavController navController;
    public SMViewModel smViewModel;
    public Utils utils;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        smViewModel = new ViewModelProvider(requireActivity()).get(SMViewModel.class);
        if (requireParentFragment() != null) {
            navController = Navigation.findNavController(requireParentFragment().getView());
        } else {
            navController = Navigation.findNavController(view);
        }
        utils = new Utils();
    }
}

package com.example.securemaps.Reportar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import com.example.securemaps.Api.ApiLugaresEtiquetados;
import com.example.securemaps.Api.ApiReportes;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogReportar.DialogReportarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.Gestionar.Lugares.Etiquetados.LugarEtiquetado;
import com.example.securemaps.Gestionar.Lugares.Etiquetados.LugaresEtiquetadosFragment;
import com.example.securemaps.Gestionar.Lugares.Favoritos.LugarFavorito;
import com.example.securemaps.R;
import com.example.securemaps.Utils.BaseDialogFragment;
import com.example.securemaps.Utils.Utils;
import com.example.securemaps.databinding.FragmentMapaPrincipalBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapaPrincipalFragment extends BaseDialogFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    private FragmentMapaPrincipalBinding binding;
    private DialogViewModel dialogViewModel;
    private GoogleMap nmap;
    private Marker lugarMarcado;
    private SearchView searchView;
    private String usuario;
    private DialogReportarFragment dialogReportarFragment;
    private ClickButtonCallback clickButtonModificarReporteInterface;
    private Location location = null;
    public LugarFavorito lugarFavorito;
    private Utils utils;
    private ReporteViewModel reporteViewModel;
    private List<Reporte> reportesList = new ArrayList<>();
    public String ciudadMostrar = null;

    public interface ClickButtonCallback {
        void click(Marker lugarMarcado);
    }

    // Constructor (obligatorio)
    public MapaPrincipalFragment() {}

    // Constructor para cuando se abre como un dialog
    public MapaPrincipalFragment(ClickButtonCallback clickButtonModificarReporteInterface) {
        this.clickButtonModificarReporteInterface = clickButtonModificarReporteInterface;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return (binding = FragmentMapaPrincipalBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchView = binding.editTextBuscar;
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        reporteViewModel = new ViewModelProvider(requireActivity()).get(ReporteViewModel.class);
        lugarMarcado = null;
        utils = new Utils();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        // Obtengo el nombre del usuario de la sesion actual
        usuario = utils.obtenerEmail();

        // Esto es para que funcione el searchview
        searchView.setQueryHint(getString(R.string.hint_editText));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String location = searchView.getQuery().toString();
                List<Address> addressList = null;
                if (location != null || !location.equals("")) {
                    Geocoder geocoder = new Geocoder(getContext());
                    try {
                        addressList = geocoder.getFromLocationName(location, 1);
                        // Siempre que se haya encontrado el lugar
                        if (addressList.size() > 0) {
                            Address address = addressList.get(0);
                            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                            nmap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        } else {
                            DynamicToast.make(requireContext(), getString(R.string.lugar_no_encontrado), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        // Click boton ubicacion
        binding.floatingButtonUbicacion.setOnClickListener(click -> {
            actualizarPosicion(location);
        });

        // Click en nuevo reporte
        binding.floatingButtonCrearReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Si se ha seleccionado un lugar, creo el reporte
                if (lugarMarcado != null) {
                    crearReporte();
                }
                // Si no hay lugar seleccionado, muestro un mensaje de error
                else {
                    DynamicToast.makeWarning(requireContext(), "Debes seleccionar un punto en el mapa para poder crear un reporte", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Muestro los lugares etiquetados en el caso de que haya
        mostrarLugaresEtiquetados();

        // Click en el boton de modificar el lugar reportado
        binding.floatingButtonModificarReporte.setOnClickListener(click -> {
            clickButtonModificarReporteInterface.click(lugarMarcado);
        });

        // Click en el boton de ubicar el lugar favorito
        binding.floatingButtonVerFavoritos.setOnClickListener(click -> {
            Location location = new Location("");
            location.setLongitude(lugarFavorito.longitude);
            location.setLatitude(lugarFavorito.latitude);
            actualizarPosicion(location);
        });


    }

    // Cuando se destruye el mapa, se pone el boton de crear reportes otra vez
    @Override
    public void onDestroy() {
        super.onDestroy();
        reporteViewModel.cambiarBotonCrearReportes.postValue(Utils.ButtonVisibility.REPORTAR);
    }

    // Click en el marcador del reporte para abrir el foro
    public void onInfoWindowClick(Marker marker) {
        clickReporte(marker);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        nmap = googleMap;
        nmap.setOnInfoWindowClickListener(this);
        // Error de permisos
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            utils.alertDialog(getActivity(), getString(R.string.error_titulo), getString(R.string.error_permisos_ubicacion), "").setPositiveButton(getString(R.string.boton_aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                    System.exit(0);
                }
            }).show();
            return;
        }
        // Obtengo la ubicacion actual y actualizo el mapa
        nmap.setMyLocationEnabled(true);
        // Quito el boton por defecto de la ubicacion
        nmap.getUiSettings().setMyLocationButtonEnabled(false);
        // Cuando cambia la ubicacion del usuario la guardo en una variable local
        nmap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location loc) {
                // Si es la primera vez que se muestra el mapa, pongo la ubicacion actual
                if (location == null) {
                    location = loc;
                    // Siempre que no haya un lugar favorito por mostrar, voy a la ubicacion del usuario
                    if (lugarFavorito == null && ciudadMostrar == null) {
                        actualizarPosicion(location);
                    }
                } else {
                    location = loc;
                }
            }
        });

        // Si tengo que mostrar un lugar favorito
        if (lugarFavorito != null) {
            Location location = new Location("");
            location.setLatitude(lugarFavorito.latitude);
            location.setLongitude(lugarFavorito.longitude);
            mostrarLugarFavorito();
            actualizarPosicion(location);
        }

        // Si tengo que mostrar una ciudad
        if (ciudadMostrar != null) {
            mostrarCiudad(ciudadMostrar);
        }

        // Click en el mapa
        nmap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                // Si es la primera vez que se clicka
                if (lugarMarcado == null) {
                    MarkerOptions a = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    lugarMarcado = nmap.addMarker(a);
                }
                // Cambio de coordenadas el marcador
                lugarMarcado.setPosition(latLng);
            }
        });

        // Obtengo la lista de reportes y los pongo en el mapa
        mostrarReportes();

        // Observo la variable de si hay que cambiar el floating button
        reporteViewModel.cambiarBotonCrearReportes.observe(getViewLifecycleOwner(), cambiarFloatingCrearReportes -> {
            // Cambia el boton de crear por el de modificar
            if (cambiarFloatingCrearReportes.equals(Utils.ButtonVisibility.MODIFICAR)) {
                binding.floatingButtonCrearReporte.setVisibility(View.GONE);
                binding.floatingButtonModificarReporte.setVisibility(View.VISIBLE);
            }
            // Cambia el boton de modificar por el de crear
            else if (cambiarFloatingCrearReportes.equals(Utils.ButtonVisibility.REPORTAR)) {
                binding.floatingButtonModificarReporte.setVisibility(View.GONE);
                binding.floatingButtonCrearReporte.setVisibility(View.VISIBLE);
            }
            // Cambia el boton de reportar por favorito
            else if (cambiarFloatingCrearReportes.equals(Utils.ButtonVisibility.FAVORITO)) {
                // Oculto
                binding.floatingButtonModificarReporte.setVisibility(View.GONE);
                binding.floatingButtonCrearReporte.setVisibility(View.GONE);
                // Muestro el boton de ubicacion del lugar favorito
                binding.floatingButtonVerFavoritos.setVisibility(View.VISIBLE);
            }
            // Si es nulo no se muestra ningun boton
            else if (cambiarFloatingCrearReportes.equals(Utils.ButtonVisibility.ALL_GONE)) {
                binding.floatingButtonModificarReporte.setVisibility(View.GONE);
                binding.floatingButtonCrearReporte.setVisibility(View.GONE);
            }
        });
    }

    // Obtengo la lista de lugares etiquetados y los pongo en el mapa
    private void mostrarLugaresEtiquetados() {
        ApiLugaresEtiquetados.api.getLugaresEtiquetados().enqueue(new Callback<ApiLugaresEtiquetados.Lugares>() {
            @Override
            public void onResponse(Call<ApiLugaresEtiquetados.Lugares> call, Response<ApiLugaresEtiquetados.Lugares> response) {
                // Siempre que no esten vacios los lugares etiquetados
                for (LugarEtiquetado lugar : response.body().results) {
                    if (lugar.latitude != 0 && lugar.longitude != 0) {
                        LatLng latLng = new LatLng(lugar.latitude, lugar.longitude);
                        // Obtengo la imagen que le pertenece a este lugar
                        nmap.addMarker(new MarkerOptions().position(latLng).title(lugar.lugar.toUpperCase()).icon(obtenerImagenLugar(lugar.lugar)).anchor(0.5f, 1));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiLugaresEtiquetados.Lugares> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Devuelve el bitmap de la imagen del lugar
    private BitmapDescriptor obtenerImagenLugar(String lugar) {
        Drawable vectorDrawable = null;
        if (lugar.equals(LugaresEtiquetadosFragment.LUGARES_DEFECTO[0])) {
            vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_home_24_maps);
        }
        else if (lugar.equals(LugaresEtiquetadosFragment.LUGARES_DEFECTO[1])) {
            vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_business_center_24); //Trabajo
        } else if (lugar.equals(LugaresEtiquetadosFragment.LUGARES_DEFECTO[2])) {
            vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_fitness_center_24); // Gimnasio
        } else if (lugar.equals(LugaresEtiquetadosFragment.LUGARES_DEFECTO[3])) {
            vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_school_24); // Escuela
       } else if (lugar.equals(LugaresEtiquetadosFragment.LUGARES_DEFECTO[4])) {
            vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_location_city_24); // Iglesia
        }
        int h = vectorDrawable.getIntrinsicHeight();
        int w = vectorDrawable.getIntrinsicWidth();
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }

    // Obtiene la calle de las cordenadas que le pasas
    public String obtenerCalle(LatLng latLng) {
        String calle = null;
        Geocoder gc = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                calle = addresses.get(0).getThoroughfare();
                System.out.println("CIUDAD: " + addresses.get(0).getLocality());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return calle;
    }

    // Obtiene la ciudad de las coordenadas que le pasas
    public String obtenerCiudad(LatLng latLng) {
        String ciudad = null;
        Geocoder gc = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = gc.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                ciudad = addresses.get(0).getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ciudad;
    }

    // Actualiza la posicion actual en el mapa
    @SuppressLint("MissingPermission")
    private void actualizarPosicion(Location location) {
        // Siempre que haya una localizacion
        if (location != null) {
            nmap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
        }
    }

    // Boton para crear un nuevo reporte
    private void crearReporte() {
        String calle = obtenerCalle(lugarMarcado.getPosition());
        String ciudad = obtenerCiudad(lugarMarcado.getPosition());
        // Si no se ha encontrado la calle
        if (calle == null) {
            DynamicToast.makeWarning(getContext(), getString(R.string.error_buscar_calle)).show();
        }
        dialogReportarFragment = new DialogReportarFragment(calle, new DialogReportarFragment.ClickDialogReportarCallback() {
            // Click boton reportar
            @Override
            public void clickButtonReportar(String comentario, boolean comentarioAnonimo) {
                double latitude = lugarMarcado.getPosition().latitude;
                double longitude = lugarMarcado.getPosition().longitude;
                Reporte nuevoReporte = new Reporte(calle, ciudad, latitude, longitude, comentario, comentarioAnonimo);
                // Inserto los datos
                ApiReportes.api.insertarReportes(nuevoReporte).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Se ha insertado con éxito
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(getContext(), getString(R.string.reporte_insertado)).show();
                            mostrarReportes();
                            // Envio la notificacion de incidentes siempre que este activado
                            if (utils.obtenerNotificacionReportes()) {
                                utils.notificarIncidentes();
                            }
                        }
                        // Error al insertar
                        else if (response.code() == 400) {
                            DynamicToast.makeError(getContext(), getString(R.string.error_registrar_reporte)).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogReportarFragment.dismiss();
            }
        });
        dialogReportarFragment.show(getChildFragmentManager(), "dialog");
    }

    // Obtiene todos los reportes y los muestra en pantalla
    private void mostrarReportes() {
        ApiReportes.api.obtenerTodosReportes().enqueue(new Callback<ApiReportes.Reportes>() {
            @Override
            public void onResponse(Call<ApiReportes.Reportes> call, Response<ApiReportes.Reportes> response) {
                reportesList = response.body().results;
                // Cada reporte lo muestro en pantalla
                for (Reporte reporte : response.body().results) {
                    LatLng latLng = new LatLng(reporte.latitude, reporte.longitude);
                    // Si el reporte es del usuario
                    if (String.valueOf(reporte.idUsuario).equals(utils.obtenerIdUsuario())) {
                        nmap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.mostrar_foro)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                    }
                    // Si el reporte es de otro usuario
                    else {
                        nmap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.mostrar_foro)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiReportes.Reportes> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Muestra en pantalla el reporte que se le pasa
    private void mostrarReporte(Reporte reporte) {
        LatLng latLng = new LatLng(reporte.latitude, reporte.longitude);
        nmap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.mostrar_foro)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
    }

    // Se muestra el lugar favorito en la direccion indicada
    public void mostrarLugarFavorito() {
        LatLng latLng = new LatLng(lugarFavorito.latitude, lugarFavorito.longitude);
        Drawable vectorDrawable = null;
        vectorDrawable = getContext().getDrawable(R.drawable.ic_baseline_favorite_24);
        int h = vectorDrawable.getIntrinsicHeight();
        int w = vectorDrawable.getIntrinsicWidth();
        vectorDrawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        vectorDrawable.draw(canvas);
        BitmapDescriptorFactory.fromBitmap(bm);
        nmap.addMarker(new MarkerOptions().position(latLng).title(lugarFavorito.calle).icon(BitmapDescriptorFactory.fromBitmap(bm)).anchor(0.5f, 1));
    }

    // Click en el marcador de un reporte
    private void clickReporte(Marker marker) {
        boolean lugarEtiquetado = false;
        // Compruebo si el marcador pertenece a un lugar etiquetado
        for (String lugar : LugaresEtiquetadosFragment.LUGARES_DEFECTO) {
            if (lugar.toUpperCase().equals(marker.getTitle())) {
                lugarEtiquetado = true;
            }
        }
        // Siempre que no sea un lugar etiquetado
        if (!lugarEtiquetado) {
            // Busco el reporte
            for (Reporte reporte : reportesList) {
                if (reporte.latitude == marker.getPosition().latitude && reporte.longitude == marker.getPosition().longitude) {
                    smViewModel.reporteComentario = reporte;
                    // Si se ha marcado un punto, lo elimino
                    if (lugarMarcado != null) {
                        lugarMarcado.remove();
                    }
                    navController.navigate(R.id.comentarioReporteFragment);
                }
            }
        }
    }

    // Muestra una ciudad en el mapa
    private void mostrarCiudad(String ciudad) {
        List<Address> addressList = null;
        Geocoder geocoder = new Geocoder(getContext());
        try {
            addressList = geocoder.getFromLocationName(ciudad, 1);
            // Siempre que se haya encontrado el lugar
            if (addressList.size() > 0) {
                Address address = addressList.get(0);
                Location location = new Location("");
                location.setLatitude(address.getLatitude());
                location.setLongitude(address.getLongitude());
                actualizarPosicion(location);

                nmap.addCircle(new CircleOptions().
                        center(new LatLng(location.getLatitude(), location.getLongitude()))
                        .radius(1000)
                        .strokeWidth(0f)
                        .fillColor(0x550000FF));
            } else {
                DynamicToast.make(requireContext(), getString(R.string.lugar_no_encontrado), Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onOptionsMenuClosed(@NonNull Menu menu) {
        super.onOptionsMenuClosed(menu);
    }
}
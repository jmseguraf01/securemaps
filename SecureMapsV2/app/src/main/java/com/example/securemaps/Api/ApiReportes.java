package com.example.securemaps.Api;

import com.example.securemaps.Reportar.Reporte;
import com.example.securemaps.Utils.Utils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ApiReportes {
    private static final String RUTA_GET = "getReportes";
    private static final String RUTA_GET_ALL = "getAllReportes";
    private static final String RUTA_GET_WITH_CALLE = "getReportesWithCalle";
    private static final String RUTA_GET_WITH_CIUDAD = "getReportesWithCiudad";
    private static final String RUTA_ADD = "addReporte";
    private static final String RUTA_UPDATE = "updateReportes";
    private static final String RUTA_DELETE = "deleteReportes";
    private static final String RUTA_NOTIFICAR_REPORTES = "notificarReportes";


    public class Reportes {
        public List<Reporte> results;
    }

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();
                            HttpUrl url = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Data.CAMPO_EMAIL, new Utils().obtenerEmail())
                                    .addQueryParameter(Data.CAMPO_PASSWORD, new Utils().obtenerPassword())
                                    .build();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .url(url);
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    }).build()
            )
            .build()
            .create(Api.class);

    public interface Api {
        @GET(RUTA_GET)
        Call<Reportes> obtenerReportes();

        @GET(RUTA_GET_ALL)
        Call<Reportes> obtenerTodosReportes();

        @GET(RUTA_GET_WITH_CALLE)
        Call<Reportes> obtenerReportes(@Query(Data.CALLE) String calle);

        @POST(RUTA_ADD)
        Call<Void> insertarReportes(@Body Reporte reporte);

        @POST(RUTA_UPDATE)
        Call<Void> updateReportes(@Body Reporte reporte);

        @GET(RUTA_DELETE)
        Call<Void> deleteReportes(@Query(Data.ID) int id);

        @GET(RUTA_GET_WITH_CIUDAD)
        Call<Reportes> obtenerReportesPorCiudad(@Query(Data.CIUDAD) String ciudad);

//        @GET(RUTA_NOTIFICAR_REPORTES)
//        Call<Void> notificarReportes();
    }
}

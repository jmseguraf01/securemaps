package com.example.securemaps.Reportar.Comentario;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.example.securemaps.Api.ApiComentario;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.Dialog.DialogModificar.DataDialogModificar;
import com.example.securemaps.Dialog.DialogModificar.DialogModificarFragment;
import com.example.securemaps.Dialog.DialogViewModel;
import com.example.securemaps.R;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.Utils.SMViewModel;
import com.example.securemaps.databinding.FragmentOpcionesComentariosBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OpcionesComentarios extends BottomSheetDialogFragment {
    private FragmentOpcionesComentariosBinding binding;
    private DialogViewModel dialogViewModel;
    private DialogModificarFragment dialogModificarFragment;
    private boolean cambios = false;

    public static OpcionesComentarios newInstance() {
        return new OpcionesComentarios();
    }

    @Override
    public void onDismiss(@NonNull @NotNull DialogInterface dialog) {
        super.onDismiss(dialog);
        // Si han habido cambios actualizo la variable
        if (cambios) {
            SMViewModel.actualizarComentarios.postValue(true);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return (binding = FragmentOpcionesComentariosBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogViewModel = new ViewModelProvider(requireActivity()).get(DialogViewModel.class);
        Comentario comentario = SMViewModel.comentarioOpciones;

        // Borrar comentario
        binding.buttonEliminar.setOnClickListener(click -> {
            cambios = true;
            borrarComentario(comentario);
        });

        // Modificar comentario
        binding.buttonModificar.setOnClickListener(click -> {
            cambios = true;
            modificarComentario(comentario);
        });
    }


    private void borrarComentario(Comentario comentario) {
        ApiComentario.api.deleteComentario(comentario).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                System.out.println(comentario.id);
                // Eliminado con exito
                if (response.code() == 200) {
                    // Actualizo los datos
                    DynamicToast.makeSuccess(getContext(), getString(R.string.comentario_eliminado)).show();
                }
                // Error al eliminar
                else {
                    DynamicToast.makeError(getContext(), getString(R.string.error_eliminar_comentario)).show();
                }
                dismiss();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    private void modificarComentario(Comentario comentario) {
        dialogModificarFragment = new DialogModificarFragment(new DialogModificarFragment.ClickDialogModificarCallback() {
            @Override
            public void clickButtonPositivo(String textoEditText) {
                comentario.comentario = textoEditText;
                ApiComentario.api.updateComentario(comentario).enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        // Actualizado con exito
                        if (response.code() == 200) {
                            DynamicToast.makeSuccess(getContext(), getString(R.string.comentario_modificado)).show();
                        }
                        // Error al actualizar
                        else {
                            DynamicToast.makeError(getContext(), getString(R.string.error_actualizar_comentario)).show();
                        }
                        dismiss();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        ErrorCode.errorApi(getContext(), t.getMessage());
                    }
                });
                dialogModificarFragment.dismiss();
            }
        });
        DataDialogModificar dataDialogModificar = new DataDialogModificar(getString(R.string.comentarios), getString(R.string.texto_comentarios), getString(R.string.modificar));
        dataDialogModificar.setColorBotonPositivo(getString(R.color.white));
        dataDialogModificar.setTextoEdittext(comentario.comentario);
        dialogViewModel.dataDialogModificar.postValue(dataDialogModificar);
        dialogModificarFragment.show(getFragmentManager(), "Dialog");
    }
}
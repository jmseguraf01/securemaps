package com.example.securemaps.Api;

import com.example.securemaps.Gestionar.Lugares.Favoritos.LugarFavorito;
import com.example.securemaps.Utils.Utils;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ApiLugaresFavoritos {
    private static final String RUTA_GET = "getlugares-favoritos";
    private static final String RUTA_INSERT = "insertlugares-favoritos";
    private static final String RUTA_DELETE = "deletelugares-favoritos";
    private static final String RUTA_UPDATE = "updatelugares-favoritos";

    public class Lugares {
        public List<LugarFavorito> results;
    }

    public static Api api = new Retrofit.Builder()
            .baseUrl(Data.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            HttpUrl originalHttpUrl = original.url();
                            HttpUrl url = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Data.CAMPO_EMAIL, new Utils().obtenerEmail())
                                    .addQueryParameter(Data.CAMPO_PASSWORD, new Utils().obtenerPassword())
                                    .build();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .url(url);
                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        }
                    }).build()
            )
            .build()
            .create(Api.class);

    public interface Api {
        @GET(RUTA_GET)
        Call<Lugares> getLugaresFavoritos();

        @POST(RUTA_INSERT)
        Call<Void> insertLugarFavorito(@Body LugarFavorito lugarFavorito);

        @POST(RUTA_UPDATE)
        Call<Void> updateLugarFavorito(@Body LugarFavorito lugarFavorito);

        @GET(RUTA_DELETE)
        Call<Void> deleteLugarFavorito(@Query(Data.ID) int id);
    }

}

package com.example.securemaps.Login;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.securemaps.Api.ApiLogin;
import com.example.securemaps.Api.ApiRegistro;
import com.example.securemaps.Api.ErrorCode;
import com.example.securemaps.R;
import com.example.securemaps.Registro.SignupTabFragment;
import com.example.securemaps.Utils.BaseFragment;
import com.example.securemaps.databinding.FragmentLoginBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.pranavpandey.android.dynamic.toasts.DynamicToast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends BaseFragment {
    private FragmentLoginBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentLoginBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Lo primero, solicito los permisos
        utils.solicitarPermisos(getContext(), getActivity());
        binding.viewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                switch (position) {
                    case 0: default:
                        return new LoginTabFragment();
                    case 1:
                        return new SignupTabFragment();
                }
            }

            @Override
            public int getItemCount() {
                return 2;
            }
        });

        new TabLayoutMediator(binding.tabLayout, binding.viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0: default:
                        tab.setText("Login");
                        break;
                    case 1:
                        tab.setText("Sign up");
                        break;

                }
            }
        }).attach();

        // Click boton entrar con google
        binding.buttonLoginGoogle.setOnClickListener(click -> {
            signInClient.launch(GoogleSignIn.getClient(getContext(), new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id)).requestEmail().build()).getSignInIntent());
        });
    }

    // Para iniciar sesion con google
    ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
        try {
            FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class).getIdToken(), null))
                    .addOnSuccessListener(task -> {
                        // Guardo el email, creo un password aleatorio y el id de google
                        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());
                        if (acct != null) {
                            String usuario = acct.getDisplayName();
                            String email = acct.getEmail();
                            String password = utils.generateString(12);
                            String idGoogle = acct.getId();
                            Uri personPhoto = acct.getPhotoUrl();
                            // Registro al usuario
                            registrarCuenta(usuario, email, password, idGoogle);
                        }
                        else {
                            DynamicToast.makeError(getContext(), getString(R.string.error_iniciar_sesion_google)).show();
                        }
                    });

        } catch (ApiException e) {}
    });

    // Registra la cuenta de google
    private void registrarCuenta(String usuario, String email, String password, String idGoogle) {
        ApiRegistro.api.add(new User(usuario, email, password, idGoogle, true)).enqueue(new Callback<ApiLogin.Respuesta>() {
            @Override
            public void onResponse(Call<ApiLogin.Respuesta> call, Response<ApiLogin.Respuesta> response) {
                // Registrado con exito
                if (response.code() == 200) {
                    // Siempre que no haya ningun error
                    if (response.body().error == null) {
                        DynamicToast.makeSuccess(getContext(), getString(R.string.usuario_registrado)).show();
                        utils.guardarUsuario(response.body().id, response.body().usuario, response.body().email, response.body().password, true);
                        utils.guardarIdGoogle(idGoogle);
                        utils.crearLugaresEtiquetados();
                        navController.navigate(R.id.go_to_MapaPrincipalFragment);
                    }
                    // El usuario ya existe, hago login
                    else if (Integer.parseInt(response.body().error) == ErrorCode.valorDuplicado) {
                        login(email, idGoogle);
                    }
                    else {
                        DynamicToast.makeError(getContext(), getString(R.string.error_registrar_usuario)).show();
                    }
                }
                // Error desconocido
                else if (response.code() == 400) {
                    DynamicToast.makeError(getContext(), getString(R.string.error_registar_usuario), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ApiLogin.Respuesta> call, Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

    // Hago login
    private void login(String email, String idGoogle) {
        ApiLogin.api.buscarGoogle(email, idGoogle).enqueue(new Callback<ApiLogin.Respuesta>() {
            @Override
            public void onResponse(@NonNull Call<ApiLogin.Respuesta> call, @NonNull Response<ApiLogin.Respuesta> response) {
                // Login correcto
                if (response.code() == 200) {
                    utils.guardarUsuario(response.body().id, response.body().usuario, response.body().email, response.body().password, response.body().notificarReporte);
                    utils.guardarIdGoogle(idGoogle);
                    navController.navigate(R.id.go_to_MapaPrincipalFragment);
                }
                // Login incorrecto
                else if (response.code() == 401) {
                    DynamicToast.makeError(getContext(), getString(R.string.usuario_incorrecto)).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApiLogin.Respuesta> call, @NonNull Throwable t) {
                ErrorCode.errorApi(getContext(), t.getMessage());
            }
        });
    }

}
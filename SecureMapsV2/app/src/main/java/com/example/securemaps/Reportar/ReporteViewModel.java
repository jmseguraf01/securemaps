package com.example.securemaps.Reportar;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.securemaps.Utils.Utils;

public class ReporteViewModel extends AndroidViewModel {
    public MutableLiveData<Utils.ButtonVisibility> cambiarBotonCrearReportes = new MutableLiveData<>();

    public ReporteViewModel(@NonNull Application application) {
        super(application);
    }
}
